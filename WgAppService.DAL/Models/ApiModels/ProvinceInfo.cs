﻿namespace WgAppService.DAL.Models.ApiModels
{
    public class ProvinceInfo
    {
        public string province_id { get; set; }
        public string province_i18n { get; set; }
    }
}
