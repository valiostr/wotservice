﻿namespace WgAppService.DAL.Models.ApiModels
{
    public class TankData
    {
        public int level { get; set; }
        public string short_name_i18n { get; set; }
        public string name_i18n { get; set; }
        public string type { get; set; }
        public int tank_id { get; set; }
        public string localized_name { get; set; }
        public string image { get; set; }
    }
}
