﻿namespace WgAppService.DAL.Models.ApiModels
{
    public class ClanMember
    {
        public long account_id { get; set; }
        public string account_name { get; set; }
        public int created_at { get; set; }
        public string role { get; set; }
        public string role_i18n { get; set; }
    }
}
