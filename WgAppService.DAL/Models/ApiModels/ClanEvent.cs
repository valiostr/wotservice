﻿using System.Collections.Generic;

namespace WgAppService.DAL.Models.ApiModels
{
    public class ClanEvent
    {
        public List<string> provinces { get; set; }
        public bool started { get; set; }
        public int time { get; set; }
        public string type { get; set; }
        public List<Arena> arenas { get; set; }
        public List<ProvinceInfo> ProvinceInfo { get; set; }
        public ClanEvent()
        {
            ProvinceInfo = new List<ProvinceInfo>();
            provinces = new List<string>();
        }
    }
}
