﻿namespace WgAppService.DAL.Models.ApiModels
{
    public class BaseUserData
    {
        public long account_id { get; set; }
        public int last_battle_time { get; set; }
        public string nickname { get; set; }
        public int updated_at { get; set; }
        public Statistics statistics { get; set; }
        public long AvgDamage { get; set; }
        public long WinRate { get; set; }
    }
}
