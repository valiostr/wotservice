﻿namespace WgAppService.DAL.Models.ApiModels
{
    public class UserInfo:BaseUserData
    {
        public Achievements achievements { get; set; }
        public string client_language { get; set; }
        public string created_at { get; set; }
        public string global_rating { get; set; }
        public int? clan_id { get; set; }
        public string logout_at { get; set; }
        public PrivateInfo @private { get; set; }
    }
}
