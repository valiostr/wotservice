﻿namespace WgAppService.DAL.Models.ApiModels
{
    public class UserTank
    {
        public long tank_id { get; set; }
        public int mark_of_mastery { get; set; }
        public ShortTankStats statistics { get; set; }
    }
}
