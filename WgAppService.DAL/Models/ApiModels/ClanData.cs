﻿using System.Collections.Generic;

namespace WgAppService.DAL.Models.ApiModels
{
    public class ClanData
    {
        public long clan_id { get; set; }
        public long members_count { get; set; }
        public string name { get; set; }
        public long owner_id { get; set; }
        public List<ClanMember> ClanMembers { get; set; }
    }
}
