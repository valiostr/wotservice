﻿
namespace WgAppService.DAL.Models.ApiModels
{
    public class Statistics
    {
        public StatisticsBase all { get; set; }
        public StatisticsBase clan { get; set; }
        public StatisticsBase company { get; set; }
        public StatisticsBase historical { get; set; }
        public int? max_xp { get; set; }
        public int? max_damage { get; set; }
        public int? max_damage_vehicle { get; set; }

    }
}
