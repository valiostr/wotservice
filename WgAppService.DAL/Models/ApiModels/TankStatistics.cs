﻿namespace WgAppService.DAL.Models.ApiModels
{
    public class TankStatistics
    {
        public StatisticsBase all { get; set; }
        public StatisticsBase clan { get; set; }
        public StatisticsBase company { get; set; }
        public long account_id { get; set; }
        public int? max_xp { get; set; }
        public int? max_frags { get; set; }
        public int? frags { get; set; }
        public int? mark_of_mastery { get; set; }
        public long tank_id { get; set; }
    }
}
