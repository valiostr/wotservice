﻿using System.Collections.Generic;

namespace WgAppService.DAL.Models.ApiModels
{
    public class PrivateInfo
    {
        public string account_type { get; set; }
        public int? gold { get; set; }
        public string free_xp { get; set; }
        public long credits { get; set; }
        public bool is_bound_to_phone { get; set; }
        public bool is_premium { get; set; }
        public string premium_expires_at { get; set; }
        public string account_type_i18n { get; set; }
        public List<long> friends { get; set; }

    }
}
