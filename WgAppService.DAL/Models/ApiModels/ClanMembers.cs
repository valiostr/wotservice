﻿using System.Collections.Generic;

namespace WgAppService.DAL.Models.ApiModels
{
    public class ClanMembers
    {
        public List<ClanMember> members { get; set; }
    }
}
