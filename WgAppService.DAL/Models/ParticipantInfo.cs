﻿
namespace WgAppService.DAL.Models
{
    public class ParticipantInfo
    {
        public long ParticipantUuid { get; set; }
        public int Role { get; set; }
    }
}
