﻿using System;
using System.Collections.Generic;

namespace WgAppService.DAL.Models
{
    public class EventModel
    {
        public Guid EventId { get; set; }
        public string Name { get; set; }
        public ParticipantModel Owner { get; set; }
        public int EventType { get; set; }
        public int StartDate { get; set; }
        public int EndDate { get; set; }
        public int LastUpdated { get; set; }
        public int RepeatType { get; set; }
        public List<int> ExtraRepeatData { get; set; }
        public List<ParticipantModel> Participants { get; set; }
        public MapModel Map { get; set; }
        public string Results { get; set; }
        public int? BattleType { get; set; }
        public bool EnableToEdit { get; set; }
        public EventModel()
        {
            ExtraRepeatData = new List<int>();
            Participants = new List<ParticipantModel>();
        }
    }
}
