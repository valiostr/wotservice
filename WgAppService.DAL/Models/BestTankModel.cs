﻿namespace WgAppService.DAL.Models
{
    public class BestTankModel
    {
        public int Uuid { get; set; }
        public string Name { get; set; }
        public string ImgUrl { get; set; }
        public int WinRate { get; set; }
        public int DamadgeRate { get; set; }
        public int ExpRate { get; set; }
        public int TankType { get; set; }
        public long Owner { get; set; }
    }
}
