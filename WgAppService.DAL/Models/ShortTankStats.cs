﻿namespace WgAppService.DAL.Models
{
    public class ShortTankStats
    {
        public int wins { get; set; }
        public int battles { get; set; }
    }
}
