﻿using System;
using System.Collections.Generic;

namespace WgAppService.DAL.Models
{
    public class CreateEventModel
    {
        public Guid EventId { get; set; }
        public long UserId { get; set; }
        public string Name { get; set; }
        public int EventType { get; set; }
        public List<ParticipantInfo> Participants { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public int RepeatType { get; set; }
        public List<int> ExtraRepeatData { get; set; }
    }
}
