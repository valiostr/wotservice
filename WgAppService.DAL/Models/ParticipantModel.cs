﻿namespace WgAppService.DAL.Models
{
    public class ParticipantModel
    {
        public long UserId { get; set; }
        public string Name { get; set; }
        public int? UserAnswer { get; set; }
        public int? AnswerTime { get; set; }
        public int PlayerRole { get; set; }
        public bool? IsInvited { get; set; }
        public bool? IsCommander { get; set; }
    }
}
