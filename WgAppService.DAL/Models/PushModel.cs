﻿namespace WgAppService.DAL.Models
{
    public class PushModel
    {
        public string Message { get; set; }
        public string Date { get; set; }

        public PushModel(string msg, string date)
        {
            Message = msg;
            Date = date;
        }
    }
}
