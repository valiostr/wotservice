﻿namespace WgAppService.DAL.Models
{
    public class MapModel
    {
        public int MapCode { get; set; }
        public string RuName { get; set; }
        public string EnName { get; set; }
    }
}
