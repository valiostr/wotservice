﻿using System.Collections.Generic;
using WgAppService.DAL.Models.ApiModels;

namespace WgAppService.DAL.Models
{
    public class SyncModel
    {
        public List<BaseUserData> ClanMembersData { get; set; }
        public List<EventModel> EventPlatoonsData { get; set; }
        public List<EventModel> EventCompanysData { get; set; }
        public List<EventModel> EventClanWarsData { get; set; }

        public SyncModel()
        {
            ClanMembersData = new List<BaseUserData>();
            EventClanWarsData = new List<EventModel>();
            EventCompanysData = new List<EventModel>();
            EventPlatoonsData = new List<EventModel>();
        }
    }
}
