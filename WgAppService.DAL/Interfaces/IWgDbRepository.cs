﻿using System;
using System.Collections.Generic;
using WgAppService.DAL.DbModels;
using WgAppService.DAL.DbModels.Dictionaries;
using WgAppService.DAL.Models;
using WgAppService.DAL.Models.ApiModels;

namespace WgAppService.DAL.Interfaces
{
    public interface IWgDbRepository
    {
        void CreateEvent(CreateEventModel eventModel);
        List<Event> CreateEvent(List<ClanEvent> events, long ownerId);
        List<Event> GetEvents(long uuid, bool onlyActive);
        Event GetEvent(Guid eventGuid);
        bool EventExists(Guid uuid);
        void UpdateEvent(CreateEventModel eventModel);
        void DropEvent(Guid eventId);
        void DropEventsForUser(long uuid);
        void CreateParticipant(string name, long participantUuid, string deviceId);
        void CreateParticipants(List<BaseUserData> participants);
        void SetUserFriends(long userId, List<long> friends);
        Participant GetParticipant(long uuid);
        List<Participant> GetParticipants(List<long> uuids);
        List<Participant> GetParticipantFriends(long userId);
        bool ParticipantsExists(List<long> uuids);
        List<EventParticipant> GetEventParticipantsByEvent(Guid eventUuid);
        List<ExtraRepeatData> GetRepeatDataByEvent(Guid eventUuid);
        void SetUserAnswer(long userUuid, Guid eventUuid, int answer);
        EventResult GetEventResult(int eventId, DateTime eventDate);
        List<long> GetAllParticipants(bool onlyWithDeviceId);
        void SetBestTank(long accountId,List<BestTankModel> tanks);
        List<BestTankModel> GetBestTanks(long userId);
    }
}
