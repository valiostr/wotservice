﻿namespace WgAppService.DAL.Interfaces
{
   public interface IWotApiRequestManager
   {
       string GetWgBaseUserData(string rowJson);
       string GetClanInfo(string rowJson);
       string GetClanEvents(string rowJson);
       string GetProvincesInfo(string rowJson);
       string GetWgTanksData(string rowJson);
       string GetWgTanksStatistics(string rowJson);
   }
}
