﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using WgAppService.DAL.DbFactoryAndContext;
using WgAppService.DAL.DbModels;
using WgAppService.DAL.DbModels.Dictionaries;
using WgAppService.DAL.Interfaces;
using WgAppService.DAL.Models;
using WgAppService.DAL.Models.ApiModels;
using WgAppService.Utilities;
using WgAppService.Utilities.Enumerations;

namespace WgAppService.DAL.Repositories
{
    public class WgDbRepository : IWgDbRepository
    {
        private readonly IDbContextFactory _contextFactory;

        public WgDbRepository()
        {
            _contextFactory = new DbContextFactory();
        }

        #region Events
        public void CreateEvent(CreateEventModel eventModel)
        {
            using (var ctx = _contextFactory.Get())
            {
                var repeter = ctx.RepeatTypes.Single(r => r.Code == eventModel.RepeatType);
                var creator = ctx.Participants.SingleOrDefault(t => t.PlayerUuid == eventModel.UserId);
                var type = ctx.EventTypes.SingleOrDefault(et => et.Code == eventModel.EventType);
                var ev = new Event
                {
                    Name = eventModel.Name,
                    EventUuid = eventModel.EventId,
                    StartDate = DateTime.Parse(eventModel.StartDate),
                    EndDate = DateTime.Parse(eventModel.EndDate),
                    RepeatType = repeter,
                    EventCreator = creator,
                    EventType = type
                };
                ctx.Events.Add(ev);
                if (eventModel.ExtraRepeatData != null && eventModel.ExtraRepeatData.Count > 0)
                {
                    List<ExtraRepeatData> repData = ctx.ExtraRepeatData.Where(u => eventModel.ExtraRepeatData.Contains(u.Code)).ToList();
                    foreach (var i in repData)
                    {
                        ctx.EventRepeat.Add(new EventRepeat { Event = ev, ExtraRepeatData = i });
                    }
                }

                foreach (var participant in eventModel.Participants)
                {
                    var part = ctx.Participants.SingleOrDefault(t => t.PlayerUuid == participant.ParticipantUuid);
                    var role = ctx.Roles.SingleOrDefault(t => t.Code == participant.Role);
                    if (part != null && role != null)
                    {
                        ctx.EventParticipant.Add(part.PlayerUuid == eventModel.UserId ? new EventParticipant
                        {
                            Event = ev,
                            Participant = creator,
                            Role = ctx.Roles.SingleOrDefault(r => r.Code == (int)RoleTypes.Main),
                            AnswerTime = DateTime.Now,
                            UserAnswerType = ctx.UserAnswerTypes.SingleOrDefault(t => t.Code == (int)UserAnswerType.Yes)
                        } : new EventParticipant { Event = ev, Participant = part, Role = role });
                    }
                }
                ctx.SaveChanges();
            }
        }
        public List<Event> CreateEvent(List<ClanEvent> events, long ownerId)
        {
            var resultEvents = new List<Event>();
            using (var ctx = _contextFactory.Get())
            {
                var repeter = ctx.RepeatTypes.Single(r => r.Code == (int)RepeateTypes.NoRepeat);
                var creator = ctx.Participants.SingleOrDefault(t => t.PlayerUuid == ownerId);
                var type = ctx.EventTypes.SingleOrDefault(et => et.Code == (int)EventTypes.ClanWar);
                foreach (var clanEvent in events)
                {
                    var provinceinf = clanEvent.ProvinceInfo.FirstOrDefault(p => p.province_id == clanEvent.provinces[0]);
                    string arenaName = clanEvent.arenas[0].name_i18n;
                    if (provinceinf != null)
                    {
                        string eventName = string.Format("{0}, {1}", provinceinf.province_i18n, arenaName);
                        if (!ctx.Events.ToList().Any(e => e.Name == eventName && e.StartDate == WgAppServiceHelper.GetDateByTimeStamp(clanEvent.time) && e.EventType == type))
                        {
                            var map = ctx.Maps.SingleOrDefault(m => m.RuName == arenaName);
                            int btype = (int)Enum.Parse(typeof(BattleTypes), clanEvent.type, true);
                            var battleType = ctx.BattleTypes.SingleOrDefault(bt => bt.Code == btype);
                            var ev = new Event
                            {
                                Name = eventName,//"provincename, arenaName"
                                EventUuid = Guid.NewGuid(),
                                StartDate = WgAppServiceHelper.GetDateByTimeStamp(clanEvent.time),
                                EndDate = WgAppServiceHelper.GetDateByTimeStamp(clanEvent.time),
                                RepeatType = repeter,
                                EventCreator = creator,
                                EventType = type,
                                Map = map,
                                BattleType = battleType,
                            };
                            resultEvents.Add(ev);
                            ctx.Events.Add(ev);
                        }
                    }
                }
                ctx.SaveChanges();
            }
            return resultEvents;
        }
        public List<Event> GetEvents(long uuid, bool onlyActive)
        {
            var evparts = _contextFactory.Get().EventParticipant.ToList().Where(ep => ep.Participant.PlayerUuid == uuid);
            var evs = new List<Event>();
            evs.AddRange(onlyActive ?
                _contextFactory.Get().Events.Where(t => t.EventCreator.PlayerUuid == uuid && t.IsActive == onlyActive).ToList()
                : _contextFactory.Get().Events.Where(t => t.EventCreator.PlayerUuid == uuid).ToList());
            foreach (var eventParticipant in evparts)
            {
                if (evs.All(eve => eve.EventUuid != eventParticipant.Event.EventUuid))
                {
                    evs.Add(eventParticipant.Event);
                }
            }
            return evs;
        }

        public Event GetEvent(Guid eventGuid)
        {
            return _contextFactory.Get().Events.SingleOrDefault(ev=>ev.EventUuid == eventGuid);
        }

        public void UpdateEvent(CreateEventModel eventModel)
        {
            bool stateChanged = false;
            using (var ctx = _contextFactory.Get())
            {
                var ev = ctx.Events.FirstOrDefault(e => e.EventUuid == eventModel.EventId);
                if (ev == null)
                    return;

                if (!string.IsNullOrEmpty(eventModel.Name) && !String.Equals(ev.Name, eventModel.Name, StringComparison.CurrentCultureIgnoreCase))
                {
                    ev.Name = eventModel.Name;
                    stateChanged = true;
                }
                if (!string.IsNullOrEmpty(eventModel.StartDate) && ev.StartDate.Date != DateTime.Parse(eventModel.StartDate).Date)
                {
                    ev.StartDate = DateTime.Parse(eventModel.StartDate);
                    stateChanged = true;
                }
                if (!string.IsNullOrEmpty(eventModel.EndDate) && ev.EndDate.Date != DateTime.Parse(eventModel.EndDate))
                {
                    ev.EndDate = DateTime.Parse(eventModel.EndDate);
                    stateChanged = true;
                }
                var rep = ctx.RepeatTypes.FirstOrDefault(t => t.Code == eventModel.RepeatType);
                if (rep != null && ev.RepeatType != rep)
                {
                    ev.RepeatType = rep;
                    stateChanged = true;
                }
                var parts = ctx.EventParticipant.Where(e => e.Event.EventUuid == eventModel.EventId);
                var roles = ctx.Roles.ToList();
                if (eventModel.Participants != null && eventModel.Participants.Count > 0)
                {
                    foreach (var part in eventModel.Participants)
                    {
                        var currPart = parts.FirstOrDefault(p => p.Participant.PlayerUuid == part.ParticipantUuid);
                        if (currPart != null && currPart.Role.Code != part.Role)
                        {
                            currPart.Role = roles.FirstOrDefault(r => r.Code == part.Role);
                            stateChanged = true;
                            ctx.EventParticipant.AddOrUpdate(currPart);
                        }
                        else
                        {
                            var participant = ctx.Participants.FirstOrDefault(p => p.PlayerUuid == part.ParticipantUuid);
                            currPart = new EventParticipant { Event = ev, Participant = participant, Role = roles.FirstOrDefault(r => r.Code == part.Role) };
                            stateChanged = true;
                            ctx.EventParticipant.Add(currPart);
                        }
                    }
                }
                if (stateChanged)
                {
                    ctx.SaveChanges();
                }
            }
        }
        public void DropEvent(Guid eventId)
        {
            using (var ctx = _contextFactory.Get())
            {
                var ev = ctx.Events.FirstOrDefault(e => e.EventUuid == eventId);
                if (ev != null)
                {
                    ctx.EventParticipant.RemoveRange(ctx.EventParticipant.ToList().Where(ep => ep.Event == ev));
                    ctx.EventRepeat.RemoveRange(ctx.EventRepeat.ToList().Where(er => er.Event == ev));
                    ctx.Events.Remove(ev);
                    ctx.SaveChanges();
                }
            }
        }
        public void DropEventsForUser(long uuid)
        {
            using (var ctx = _contextFactory.Get())
            {
                var part = ctx.Participants.SingleOrDefault(e => e.PlayerUuid == uuid);
                if (part != null)
                {
                    var events = ctx.Events.ToList().Where(p => p.EventCreator.Id == part.Id).ToList();
                    var evparts = new List<EventParticipant>();
                    foreach (var @event in events)
                    {
                        evparts.AddRange(ctx.EventParticipant.ToList().Where(ep => ep.Event.EventUuid == @event.EventUuid).ToList());
                    }
                    var evreps = new List<EventRepeat>();
                    foreach (var @event in events)
                    {
                        evreps.AddRange(ctx.EventRepeat.ToList().Where(ep => ep.Event.EventUuid == @event.EventUuid).ToList());
                    }
                    ctx.EventParticipant.RemoveRange(evparts);
                    ctx.EventRepeat.RemoveRange(evreps);
                    ctx.Events.RemoveRange(events);
                }
            }
        }
        public bool EventExists(Guid uuid)
        {
            using (var ctx = _contextFactory.Get())
            {
                return ctx.Events.Any(e => e.EventUuid == uuid);
            }
        }
        public EventResult GetEventResult(int eventId, DateTime eventDate)
        {
            return _contextFactory.Get().EventResults.ToList().SingleOrDefault(e => e.Event.Id == eventId && e.EventDate == eventDate);
        }

        #endregion

        #region Participants

        public void CreateParticipant(string name, long participantUuid, string deviceId)
        {
            using (var ctx = _contextFactory.Get())
            {
                bool changed = false;
                if (!ctx.Participants.Any(p => p.PlayerUuid == participantUuid))
                {
                    var part = new Participant { Name = name, PlayerUuid = participantUuid, DeviceId = deviceId };
                    ctx.Participants.Add(part);
                    changed = true;
                }
                else
                {
                    var part = ctx.Participants.SingleOrDefault(p => p.PlayerUuid == participantUuid);
                    if (part != null && (string.IsNullOrEmpty(part.DeviceId) || part.DeviceId != deviceId))
                    {
                        part.DeviceId = deviceId;
                        ctx.Participants.AddOrUpdate(part);
                        changed = true;
                    }
                }
                if (changed)
                {
                    ctx.SaveChanges();
                }
            }
        }
        public void SetUserFriends(long userId, List<long> friends)
        {
            using (var ctx = _contextFactory.Get())
            {
                var part = ctx.Participants.SingleOrDefault(t => t.PlayerUuid == userId);

                if (part == null || friends == null || !friends.Any())
                    return;

                bool changed = false;
                friends.Remove(userId);
                var partFriends = ctx.ParticipantFriends.ToList();
                if (partFriends.Count > friends.Count)
                {
                    changed = true;
                    var removedFriends = partFriends.Where(pf => !friends.Contains(pf.Friend.PlayerUuid) && pf.Participant.PlayerUuid == part.PlayerUuid).ToList();
                    ctx.ParticipantFriends.RemoveRange(removedFriends);
                }
                foreach (var friend in friends)
                {
                    if (!partFriends.Any(t => t.Friend.PlayerUuid == friend && t.Participant.PlayerUuid == part.PlayerUuid))
                    {
                        changed = true;
                        var friendEnt = ctx.Participants.SingleOrDefault(f => f.PlayerUuid == friend);
                        if (friendEnt != null)
                        {
                            ctx.ParticipantFriends.Add(new ParticipantFriend { Participant = part, Friend = friendEnt });
                        }
                    }
                }
                if (changed)
                {
                    ctx.SaveChanges();
                }
            }
        }
        public void CreateParticipants(List<BaseUserData> participants)
        {
            using (var ctx = _contextFactory.Get())
            {
                participants = participants.Where(baseUserData => !ctx.Participants.Any(t => t.PlayerUuid == baseUserData.account_id)).ToList();
                if (participants.Count > 0)
                {
                    foreach (BaseUserData baseUserData in participants)
                    {
                        ctx.Participants.Add(new Participant
                        {
                            Name = baseUserData.nickname,
                            PlayerUuid = baseUserData.account_id
                        });
                    }
                    ctx.SaveChanges();
                }
            }
        }
        public Participant GetParticipant(long uuid)
        {
            return _contextFactory.Get().Participants.FirstOrDefault(u => u.PlayerUuid == uuid);
        }
        public List<Participant> GetParticipants(List<long> uuids)
        {
            return _contextFactory.Get().Participants.Where(u => uuids.Contains(u.PlayerUuid)).ToList();
        }
        public List<Participant> GetParticipantFriends(long userId)
        {
            List<Participant> friends = _contextFactory.Get().ParticipantFriends.ToList().Where(p => p.Participant.PlayerUuid == userId).Select(t => t.Friend).ToList();
            return friends;
        }
        public bool ParticipantsExists(List<long> uuids)
        {
            using (var ctx = _contextFactory.Get())
            {
                if (uuids.Select(uuid => ctx.Participants.FirstOrDefault(p => p.PlayerUuid == uuid)).Any(user => user == null))
                {
                    return false;
                }
            }
            return true;
        }
        public List<long> GetAllParticipants(bool onlyWithDeviceId)
        {
            var users = new List<long>();
            using (var ctx = _contextFactory.Get())
            {
                users.AddRange(onlyWithDeviceId
                    ? ctx.Participants.Where(p => !string.IsNullOrEmpty(p.DeviceId)).Select(p => p.PlayerUuid)
                    : ctx.Participants.ToList().Select(p => p.PlayerUuid));
            }
            return users;
        }

        #endregion

        #region repeatData
        public List<ExtraRepeatData> GetExtraRepeatData(List<int> extraRepeatData)
        {
            if (extraRepeatData != null && extraRepeatData.Count > 0)
            {
                return _contextFactory.Get().ExtraRepeatData.Where(u => extraRepeatData.Contains(u.Code)).ToList();
            }
            return new List<ExtraRepeatData>();
        }
        public ExtraRepeatData GetExtraRepeatData(int extraRepeatDataCode)
        {
            return _contextFactory.Get().ExtraRepeatData.FirstOrDefault(u => u.Code == extraRepeatDataCode);
        }
        #endregion

        #region eventParticipants
        public List<EventParticipant> GetEventParticipantsByEvent(Guid eventUuid)
        {
            return _contextFactory.Get().EventParticipant.Where(t => t.Event.EventUuid == eventUuid).ToList();
        }
        public void SetUserAnswer(long userUuid, Guid eventUuid, int answer)
        {
            using (var ctx = _contextFactory.Get())
            {
                var evpart = ctx.EventParticipant.FirstOrDefault(ev => ev.Event.EventUuid == eventUuid && ev.Participant.PlayerUuid == userUuid);

                if (evpart == null)
                    return;

                evpart.AnswerTime = DateTime.Now.Date;
                evpart.UserAnswerType = ctx.UserAnswerTypes.FirstOrDefault(at => at.Code == answer);
                ctx.EventParticipant.AddOrUpdate(evpart);
                ctx.SaveChanges();
            }
        }

        #endregion

        #region extraRepeatData

        public List<ExtraRepeatData> GetRepeatDataByEvent(Guid eventUuid)
        {
            return _contextFactory.Get().EventRepeat.Where(t => t.Event.EventUuid == eventUuid).Select(t => t.ExtraRepeatData).ToList();
        }

        #endregion

        #region Tanks

        public void SetBestTank(long accountId, List<BestTankModel> tanks)
        {
            using (var ctx = _contextFactory.Get())
            {
                var bestSpg = tanks.Where(tt => tt.TankType == (int)TankTypes.Spg).OrderByDescending(t => t.DamadgeRate).ThenBy(t => t.WinRate).ThenBy(t => t.ExpRate).FirstOrDefault();
                var bestAtSpg = tanks.Where(tt => tt.TankType == (int)TankTypes.AtSpg).OrderByDescending(t => t.DamadgeRate).ThenBy(t => t.WinRate).ThenBy(t => t.ExpRate).FirstOrDefault();
                var bestlt = tanks.Where(tt => tt.TankType == (int)TankTypes.LightTank).OrderByDescending(t => t.DamadgeRate).ThenBy(t => t.WinRate).ThenBy(t => t.ExpRate).FirstOrDefault();
                var bestMt = tanks.Where(tt => tt.TankType == (int)TankTypes.MediumTank).OrderByDescending(t => t.DamadgeRate).ThenBy(t => t.WinRate).ThenBy(t => t.ExpRate).FirstOrDefault();
                var bestHt = tanks.Where(tt => tt.TankType == (int)TankTypes.HeavyTank).OrderByDescending(t => t.DamadgeRate).ThenBy(t => t.WinRate).ThenBy(t => t.ExpRate).FirstOrDefault();

                var owner = ctx.Participants.SingleOrDefault(p => p.PlayerUuid == accountId);
                if (bestSpg != null)
                {
                    var type = ctx.TankTypes.SingleOrDefault(tt => tt.Code == bestSpg.TankType);
                    var currbestSpg = ctx.BestTanks.SingleOrDefault(tt => tt.Owner.PlayerUuid == accountId && tt.TankType.Id == type.Id);
                    if (currbestSpg == null)
                    {
                        currbestSpg = new TankInfo
                        {
                            DamadgeRate = bestSpg.DamadgeRate,
                            ExpRate = bestSpg.ExpRate,
                            ImgUrl = bestSpg.ImgUrl,
                            Name = bestSpg.Name,
                            Owner = owner,
                            TankType = type,
                            Uuid = bestSpg.Uuid,
                            WinRate = bestSpg.WinRate
                        };
                    }
                    else if (currbestSpg.Uuid != bestSpg.Uuid)
                    {
                        currbestSpg.DamadgeRate = bestSpg.DamadgeRate;
                        currbestSpg.ExpRate = bestSpg.ExpRate;
                        currbestSpg.ImgUrl = bestSpg.ImgUrl;
                        currbestSpg.TankType = type;
                        currbestSpg.Name = bestSpg.Name;
                        currbestSpg.Uuid = bestSpg.Uuid;
                        currbestSpg.WinRate = bestSpg.WinRate;
                    }
                    ctx.BestTanks.AddOrUpdate(currbestSpg);
                }
                if (bestAtSpg != null)
                {
                    var type = ctx.TankTypes.SingleOrDefault(tt => tt.Code == bestAtSpg.TankType);
                    var currbestAtSpg = ctx.BestTanks.SingleOrDefault(tt => tt.Owner.PlayerUuid == accountId && tt.TankType.Id == type.Id);
                    if (currbestAtSpg == null)
                    {
                        currbestAtSpg = new TankInfo
                        {
                            DamadgeRate = bestAtSpg.DamadgeRate,
                            ExpRate = bestAtSpg.ExpRate,
                            ImgUrl = bestAtSpg.ImgUrl,
                            Name = bestAtSpg.Name,
                            Owner = owner,
                            TankType = type,
                            Uuid = bestAtSpg.Uuid,
                            WinRate = bestAtSpg.WinRate
                        };
                    }
                    else if (currbestAtSpg.Uuid != bestAtSpg.Uuid)
                    {
                        currbestAtSpg.DamadgeRate = bestAtSpg.DamadgeRate;
                        currbestAtSpg.ExpRate = bestAtSpg.ExpRate;
                        currbestAtSpg.ImgUrl = bestAtSpg.ImgUrl;
                        currbestAtSpg.TankType = type;
                        currbestAtSpg.Name = bestAtSpg.Name;
                        currbestAtSpg.Uuid = bestAtSpg.Uuid;
                        currbestAtSpg.WinRate = bestAtSpg.WinRate;
                    }
                    ctx.BestTanks.AddOrUpdate(currbestAtSpg);
                }
                if (bestlt != null)
                {
                    var type = ctx.TankTypes.SingleOrDefault(tt => tt.Code == bestlt.TankType);
                    var currbestLt = ctx.BestTanks.SingleOrDefault(tt => tt.Owner.PlayerUuid == accountId && tt.TankType.Id == type.Id);
                    if (currbestLt == null)
                    {
                        currbestLt = new TankInfo
                        {
                            DamadgeRate = bestlt.DamadgeRate,
                            ExpRate = bestlt.ExpRate,
                            ImgUrl = bestlt.ImgUrl,
                            Name = bestlt.Name,
                            Owner = owner,
                            TankType = type,
                            Uuid = bestlt.Uuid,
                            WinRate = bestlt.WinRate
                        };
                    }
                    else if (currbestLt.Uuid != bestlt.Uuid)
                    {
                        currbestLt.DamadgeRate = bestlt.DamadgeRate;
                        currbestLt.ExpRate = bestlt.ExpRate;
                        currbestLt.ImgUrl = bestlt.ImgUrl;
                        currbestLt.TankType = type;
                        currbestLt.Name = bestlt.Name;
                        currbestLt.Uuid = bestlt.Uuid;
                        currbestLt.WinRate = bestlt.WinRate;
                    }
                    ctx.BestTanks.AddOrUpdate(currbestLt);
                }
                if (bestMt != null)
                {
                    var type = ctx.TankTypes.SingleOrDefault(tt => tt.Code == bestMt.TankType);
                    var currbestMt = ctx.BestTanks.SingleOrDefault(tt => tt.Owner.PlayerUuid == accountId && tt.TankType.Id == type.Id);
                    if (currbestMt == null)
                    {
                        currbestMt = new TankInfo
                        {
                            DamadgeRate = bestMt.DamadgeRate,
                            ExpRate = bestMt.ExpRate,
                            ImgUrl = bestMt.ImgUrl,
                            Name = bestMt.Name,
                            Owner = owner,
                            TankType = type,
                            Uuid = bestMt.Uuid,
                            WinRate = bestMt.WinRate
                        };
                    }
                    else if (currbestMt.Uuid != bestMt.Uuid)
                    {
                        currbestMt.DamadgeRate = bestMt.DamadgeRate;
                        currbestMt.ExpRate = bestMt.ExpRate;
                        currbestMt.ImgUrl = bestMt.ImgUrl;
                        currbestMt.TankType = type;
                        currbestMt.Name = bestMt.Name;
                        currbestMt.Uuid = bestMt.Uuid;
                        currbestMt.WinRate = bestMt.WinRate;
                    }
                    ctx.BestTanks.AddOrUpdate(currbestMt);
                }

                if (bestHt != null)
                {
                    var type = ctx.TankTypes.SingleOrDefault(tt => tt.Code == bestHt.TankType);
                    var currbestHt = ctx.BestTanks.SingleOrDefault(tt => tt.Owner.PlayerUuid == accountId && tt.TankType.Id == type.Id);
                    if (currbestHt == null)
                    {
                        currbestHt = new TankInfo
                        {
                            DamadgeRate = bestHt.DamadgeRate,
                            ExpRate = bestHt.ExpRate,
                            ImgUrl = bestHt.ImgUrl,
                            Name = bestHt.Name,
                            Owner = owner,
                            TankType = type,
                            Uuid = bestHt.Uuid,
                            WinRate = bestHt.WinRate
                        };
                    }
                    else if (currbestHt.Uuid != bestHt.Uuid)
                    {
                        currbestHt.DamadgeRate = bestHt.DamadgeRate;
                        currbestHt.ExpRate = bestHt.ExpRate;
                        currbestHt.ImgUrl = bestHt.ImgUrl;
                        currbestHt.TankType = type;
                        currbestHt.Name = bestHt.Name;
                        currbestHt.Uuid = bestHt.Uuid;
                        currbestHt.WinRate = bestHt.WinRate;
                    }
                    ctx.BestTanks.AddOrUpdate(currbestHt);
                }
                ctx.SaveChanges();
            }
        }
        public List<BestTankModel> GetBestTanks(long userId)
        {
            var items = _contextFactory.Get().BestTanks.ToList().Where(t => t.Owner.PlayerUuid == userId).ToList();
            return items.Select(tankInfo => new BestTankModel { DamadgeRate = tankInfo.DamadgeRate, ExpRate = tankInfo.ExpRate, ImgUrl = tankInfo.ImgUrl, TankType = tankInfo.TankType.Code, Name = tankInfo.Name, Owner = tankInfo.Owner.PlayerUuid, Uuid = tankInfo.Uuid, WinRate = tankInfo.WinRate }).ToList();
        }

        #endregion
    }
}
