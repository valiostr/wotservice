﻿namespace WgAppService.DAL.DbFactoryAndContext
{
    public interface IDbContextFactory
    {
        WgDbContext Get();
    }
}