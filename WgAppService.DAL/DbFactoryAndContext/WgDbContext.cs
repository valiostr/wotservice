﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using WgAppService.DAL.DbModels;
using WgAppService.DAL.DbModels.Dictionaries;

namespace WgAppService.DAL.DbFactoryAndContext
{
    public class WgDbContext : DbContext
    {
        public WgDbContext()
            : base("WgConnection")
        {

        }
        public DbSet<ExtraRepeatData> ExtraRepeatData { get; set; }
        public DbSet<PlayerRole> Roles { get; set; }
        public DbSet<RepeatType> RepeatTypes { get; set; }
        public DbSet<UserAnswer> UserAnswerTypes { get; set; }
        public DbSet<EventType> EventTypes { get; set; }
        public DbSet<BattleType> BattleTypes { get; set; }
        public DbSet<TankType> TankTypes { get; set; }
        public DbSet<Map> Maps { get; set; }
        public DbSet<Participant> Participants { get; set; }
        public DbSet<Event> Events { get; set; }
        public DbSet<ParticipantFriend> ParticipantFriends { get; set; }
        public DbSet<EventParticipant> EventParticipant { get; set; }
        public DbSet<EventResult> EventResults { get; set; }
        public DbSet<EventRepeat> EventRepeat { get; set; }
        public DbSet<TankInfo> BestTanks { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}
