﻿namespace WgAppService.DAL.DbFactoryAndContext
{
    public class DbContextFactory : IDbContextFactory
    {
        public WgDbContext Get()
        {
            var ctx = new WgDbContext();
            return ctx;
        }
    }
}
