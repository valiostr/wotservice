using System.Collections.Generic;
using System.Data.Entity.Migrations;
using WgAppService.DAL.DbFactoryAndContext;
using WgAppService.DAL.DbModels.Dictionaries;

namespace WgAppService.DAL.Migrations
{
    internal sealed class Configuration : DbMigrationsConfiguration<WgDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(WgDbContext context)
        {
            context.RepeatTypes.AddRange(new List<RepeatType>
            {
                new RepeatType {Code = 0, Description = "No repeat"},
                new RepeatType {Code = 1, Description = "Daily"},
                new RepeatType {Code = 2, Description = "Weekly"},
                new RepeatType {Code = 3, Description = "Every 2 weeks"},
                new RepeatType {Code = 4, Description = "Monthly"},
                new RepeatType {Code = 5, Description = "Every 6 months"},
                new RepeatType {Code = 6, Description = "Every year"}
            });

            context.ExtraRepeatData.AddRange(new List<ExtraRepeatData>
            {
                new ExtraRepeatData { Code = 0, Description = "Monday" },
                new ExtraRepeatData { Code = 1, Description = "Tuesday" },
                new ExtraRepeatData { Code = 2, Description = "Wednesday" },
                new ExtraRepeatData { Code = 3, Description = "Thursday" },
                new ExtraRepeatData { Code = 4, Description = "Friday" },
                new ExtraRepeatData { Code = 5, Description = "Satudray" },
                new ExtraRepeatData { Code = 6, Description = "Sanday" }
            });

            context.Roles.AddRange(new List<PlayerRole>
            {
                new PlayerRole {Code = 0, Description = "Main"},
                new PlayerRole {Code = 1, Description = "Secondary"}
            });

            context.TankTypes.AddRange(new List<TankType>
            {
                new TankType { Code = 0, Description = "SPG" },
                new TankType { Code = 1, Description = "Light Tank" },
                new TankType { Code = 2, Description = "Medium tank" },
                new TankType { Code = 3, Description = "Heavy tank" },
                new TankType { Code = 4, Description = "AT-SPG" }
            });

            context.UserAnswerTypes.AddRange(new List<UserAnswer>
            {
                new UserAnswer { Code = 0, Description = "Yes" },
                new UserAnswer { Code = 1, Description = "No" },
                new UserAnswer { Code = 2, Description = "Maybe" }
            });

            context.EventTypes.AddRange(new List<EventType>
            {
                new EventType { Code = 0, Description = "Platoon" },
                new EventType { Code = 1, Description = "�ompany" },
                new EventType { Code = 2, Description = "Clan" },
                new EventType { Code = 3, Description = "Training" },
            });

            context.BattleTypes.AddRange(new List<BattleType>
            {
                new BattleType { Code = 0, Description = "Landing" },
                new BattleType { Code = 1, Description = "MeetingEngagement" },
                new BattleType { Code = 2, Description = "ForProvince" }
            });

            context.Maps.AddRange(new List<Map>
            {
                new Map { Code = 0, Description = "Map", RuName = "��������",EnName = "Airfield"},
                new Map { Code = 1, Description = "Map", RuName = "���-������",EnName = "El Halluf" },
                new Map { Code = 2, Description = "Map", RuName = "���������",EnName = "Erlenberg" },
                new Map { Code = 3, Description = "Map", RuName = "�������� �����",EnName = "Fisherman's Bay" },
                new Map { Code = 4, Description = "Map", RuName = "�������",EnName = "Karelia" },
                new Map { Code = 5, Description = "Map", RuName = "�������",EnName = "Komarin" },
                new Map { Code = 6, Description = "Map", RuName = "���������",EnName = "Malinovka" },
                new Map { Code = 7, Description = "Map", RuName = "���������",EnName = "Murovanka" },
                new Map { Code = 8, Description = "Map", RuName = "����������",EnName = "Prokhorovka" },
                new Map { Code = 9, Description = "Map", RuName = "������",EnName = "Redshire" },
                new Map { Code = 10, Description = "Map", RuName = "�������� ����",EnName = "Sand River" },
                new Map { Code = 11, Description = "Map", RuName = "����� �����",EnName = "Serene Coast" },
                new Map { Code = 12, Description = "Map", RuName = "����� �����",EnName = "South Coast" },
                new Map { Code = 13, Description = "Map", RuName = "�����",EnName = "Steppes" },
                new Map { Code = 14, Description = "Map", RuName = "����",EnName = "Swamp" },
                new Map { Code = 15, Description = "Map", RuName = "������",EnName = "Tundra" },
                new Map { Code = 16, Description = "Map", RuName = "��������",EnName = "Westfield" },
                new Map { Code = 17, Description = "Map", RuName = "������������",EnName = "Himmelsdorf" },
                new Map { Code = 18, Description = "Map", RuName = "���������",EnName = "Abbey" },
                new Map { Code = 19, Description = "Map", RuName = "���������",EnName = "Arctic Region" },
                new Map { Code = 20, Description = "Map", RuName = "���",EnName = "Cliff" },
                new Map { Code = 21, Description = "Map", RuName = "������ �������",EnName = "Dragon Ridge" },
                new Map { Code = 22, Description = "Map", RuName = "����",EnName = "Ensk" },
                new Map { Code = 23, Description = "Map", RuName = "������",EnName = "Fjords" },
                new Map { Code = 24, Description = "Map", RuName = "��������",EnName = "Lakeville" },
                new Map { Code = 25, Description = "Map", RuName = "���� ���",EnName = "Live Oaks" },
                new Map { Code = 26, Description = "Map", RuName = "������",EnName = "Highway" },
                new Map { Code = 27, Description = "Map", RuName = "�������",EnName = "Mines" },
                new Map { Code = 28, Description = "Map", RuName = "�������",EnName = "Mountain Pass" },
                new Map { Code = 29, Description = "Map", RuName = "��������� ����",EnName = "Pearl River" },
                new Map { Code = 30, Description = "Map", RuName = "����",EnName = "Port" },
                new Map { Code = 31, Description = "Map", RuName = "���������",EnName = "Province" },
                new Map { Code = 32, Description = "Map", RuName = "��������",EnName = "Ruinberg" },
                new Map { Code = 33, Description = "Map", RuName = "��������� ������",EnName = "Sacred Valley" },
                new Map { Code = 34, Description = "Map", RuName = "�����������",EnName = "Severogorsk" },
                new Map { Code = 35, Description = "Map", RuName = "����� ��������",EnName = "Siegfried Line" },
                new Map { Code = 36, Description = "Map", RuName = "��������",EnName = "Widepark" },
                new Map { Code = 37, Description = "Map", RuName = "�������",EnName = "Kharkov" },
                new Map { Code = 38, Description = "Map", RuName = "�������� ����",EnName = "Fire Arc" },
                new Map { Code = 39, Description = "Map", RuName = "�������� � ����",EnName = "Ruinberg under fire" },
                new Map { Code = 40, Description = "Map", RuName = "������-�����",EnName = "Northwest" },
                new Map { Code = 41, Description = "Map", RuName = "������� �������",EnName = "Hidden ville" },
                new Map { Code = 42, Description = "Map", RuName = "���������",EnName = "Windstorm" },
                new Map { Code = 43, Description = "Map", RuName = "������ ������������",EnName = "Winter Himmelsdorf" }
            });

            context.SaveChanges();
        }
    }
}
