namespace WgAppService.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.BattleType",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Code = c.Int(nullable: false),
                        Description = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.TankInfo",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Uuid = c.Int(nullable: false),
                        Name = c.String(),
                        ImgUrl = c.String(),
                        WinRate = c.Int(nullable: false),
                        DamadgeRate = c.Int(nullable: false),
                        ExpRate = c.Int(nullable: false),
                        CreateDate = c.DateTime(nullable: false),
                        Owner_Id = c.Int(),
                        TankType_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Participant", t => t.Owner_Id)
                .ForeignKey("dbo.TankType", t => t.TankType_Id)
                .Index(t => t.Owner_Id)
                .Index(t => t.TankType_Id);
            
            CreateTable(
                "dbo.Participant",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PlayerUuid = c.Long(nullable: false),
                        Name = c.String(),
                        DeviceId = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.TankType",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Code = c.Int(nullable: false),
                        Description = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.EventParticipant",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        AnswerTime = c.DateTime(),
                        IsInvited = c.Boolean(),
                        IsCommander = c.Boolean(),
                        Event_Id = c.Int(),
                        Participant_Id = c.Int(),
                        Role_Id = c.Int(),
                        UserAnswerType_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Event", t => t.Event_Id)
                .ForeignKey("dbo.Participant", t => t.Participant_Id)
                .ForeignKey("dbo.PlayerRole", t => t.Role_Id)
                .ForeignKey("dbo.UserAnswer", t => t.UserAnswerType_Id)
                .Index(t => t.Event_Id)
                .Index(t => t.Participant_Id)
                .Index(t => t.Role_Id)
                .Index(t => t.UserAnswerType_Id);
            
            CreateTable(
                "dbo.Event",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        EventUuid = c.Guid(nullable: false),
                        Name = c.String(),
                        StartDate = c.DateTime(nullable: false),
                        EndDate = c.DateTime(nullable: false),
                        CreateDate = c.DateTime(nullable: false),
                        LastUpdated = c.DateTime(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        BattleType_Id = c.Int(),
                        EventCreator_Id = c.Int(),
                        EventType_Id = c.Int(),
                        Map_Id = c.Int(),
                        RepeatType_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.BattleType", t => t.BattleType_Id)
                .ForeignKey("dbo.Participant", t => t.EventCreator_Id)
                .ForeignKey("dbo.EventType", t => t.EventType_Id)
                .ForeignKey("dbo.Map", t => t.Map_Id)
                .ForeignKey("dbo.RepeatType", t => t.RepeatType_Id)
                .Index(t => t.BattleType_Id)
                .Index(t => t.EventCreator_Id)
                .Index(t => t.EventType_Id)
                .Index(t => t.Map_Id)
                .Index(t => t.RepeatType_Id);
            
            CreateTable(
                "dbo.EventType",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Code = c.Int(nullable: false),
                        Description = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Map",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        RuName = c.String(),
                        EnName = c.String(),
                        Code = c.Int(nullable: false),
                        Description = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.RepeatType",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Code = c.Int(nullable: false),
                        Description = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.PlayerRole",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Code = c.Int(nullable: false),
                        Description = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.UserAnswer",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Code = c.Int(nullable: false),
                        Description = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.EventRepeat",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Event_Id = c.Int(),
                        ExtraRepeatData_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Event", t => t.Event_Id)
                .ForeignKey("dbo.ExtraRepeatData", t => t.ExtraRepeatData_Id)
                .Index(t => t.Event_Id)
                .Index(t => t.ExtraRepeatData_Id);
            
            CreateTable(
                "dbo.ExtraRepeatData",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Code = c.Int(nullable: false),
                        Description = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.EventResult",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        BattleResults = c.String(),
                        IsLost = c.Boolean(),
                        EventDate = c.DateTime(nullable: false),
                        Event_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Event", t => t.Event_Id)
                .Index(t => t.Event_Id);
            
            CreateTable(
                "dbo.ParticipantFriend",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Friend_Id = c.Int(),
                        Participant_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Participant", t => t.Friend_Id)
                .ForeignKey("dbo.Participant", t => t.Participant_Id)
                .Index(t => t.Friend_Id)
                .Index(t => t.Participant_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ParticipantFriend", "Participant_Id", "dbo.Participant");
            DropForeignKey("dbo.ParticipantFriend", "Friend_Id", "dbo.Participant");
            DropForeignKey("dbo.EventResult", "Event_Id", "dbo.Event");
            DropForeignKey("dbo.EventRepeat", "ExtraRepeatData_Id", "dbo.ExtraRepeatData");
            DropForeignKey("dbo.EventRepeat", "Event_Id", "dbo.Event");
            DropForeignKey("dbo.EventParticipant", "UserAnswerType_Id", "dbo.UserAnswer");
            DropForeignKey("dbo.EventParticipant", "Role_Id", "dbo.PlayerRole");
            DropForeignKey("dbo.EventParticipant", "Participant_Id", "dbo.Participant");
            DropForeignKey("dbo.EventParticipant", "Event_Id", "dbo.Event");
            DropForeignKey("dbo.Event", "RepeatType_Id", "dbo.RepeatType");
            DropForeignKey("dbo.Event", "Map_Id", "dbo.Map");
            DropForeignKey("dbo.Event", "EventType_Id", "dbo.EventType");
            DropForeignKey("dbo.Event", "EventCreator_Id", "dbo.Participant");
            DropForeignKey("dbo.Event", "BattleType_Id", "dbo.BattleType");
            DropForeignKey("dbo.TankInfo", "TankType_Id", "dbo.TankType");
            DropForeignKey("dbo.TankInfo", "Owner_Id", "dbo.Participant");
            DropIndex("dbo.ParticipantFriend", new[] { "Participant_Id" });
            DropIndex("dbo.ParticipantFriend", new[] { "Friend_Id" });
            DropIndex("dbo.EventResult", new[] { "Event_Id" });
            DropIndex("dbo.EventRepeat", new[] { "ExtraRepeatData_Id" });
            DropIndex("dbo.EventRepeat", new[] { "Event_Id" });
            DropIndex("dbo.Event", new[] { "RepeatType_Id" });
            DropIndex("dbo.Event", new[] { "Map_Id" });
            DropIndex("dbo.Event", new[] { "EventType_Id" });
            DropIndex("dbo.Event", new[] { "EventCreator_Id" });
            DropIndex("dbo.Event", new[] { "BattleType_Id" });
            DropIndex("dbo.EventParticipant", new[] { "UserAnswerType_Id" });
            DropIndex("dbo.EventParticipant", new[] { "Role_Id" });
            DropIndex("dbo.EventParticipant", new[] { "Participant_Id" });
            DropIndex("dbo.EventParticipant", new[] { "Event_Id" });
            DropIndex("dbo.TankInfo", new[] { "TankType_Id" });
            DropIndex("dbo.TankInfo", new[] { "Owner_Id" });
            DropTable("dbo.ParticipantFriend");
            DropTable("dbo.EventResult");
            DropTable("dbo.ExtraRepeatData");
            DropTable("dbo.EventRepeat");
            DropTable("dbo.UserAnswer");
            DropTable("dbo.PlayerRole");
            DropTable("dbo.RepeatType");
            DropTable("dbo.Map");
            DropTable("dbo.EventType");
            DropTable("dbo.Event");
            DropTable("dbo.EventParticipant");
            DropTable("dbo.TankType");
            DropTable("dbo.Participant");
            DropTable("dbo.TankInfo");
            DropTable("dbo.BattleType");
        }
    }
}
