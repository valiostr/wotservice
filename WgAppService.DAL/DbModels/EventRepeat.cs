﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using WgAppService.DAL.DbModels.Dictionaries;

namespace WgAppService.DAL.DbModels
{
   public class EventRepeat
    {
       [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
       public int Id { get; set; }
       public virtual Event Event { get; set; }
       public virtual ExtraRepeatData ExtraRepeatData { get; set; }
    }
}
