﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using WgAppService.DAL.DbModels.Dictionaries;

namespace WgAppService.DAL.DbModels
{
    public class TankInfo
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public int Uuid { get; set; }
        public string Name { get; set; }
        public string ImgUrl { get; set; }
        public int WinRate { get; set; }
        public int DamadgeRate { get; set; }
        public int ExpRate { get; set; }
        public virtual TankType TankType { get; set; }
        public virtual Participant Owner { get; set; }
        public DateTime CreateDate { get; set; }

        public TankInfo()
        {
            CreateDate = DateTime.Now;
        }
    }
}
