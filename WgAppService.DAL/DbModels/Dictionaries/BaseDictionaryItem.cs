﻿using System.ComponentModel.DataAnnotations.Schema;

namespace WgAppService.DAL.DbModels.Dictionaries
{
    public class BaseDictionaryItem
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public int Code { get; set; }
        public string Description { get; set; }
    }
}
