﻿namespace WgAppService.DAL.DbModels.Dictionaries
{
    public class Map:BaseDictionaryItem
    {
        public string RuName { get; set; }
        public string EnName { get; set; }
    }
}
