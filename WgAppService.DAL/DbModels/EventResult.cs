﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace WgAppService.DAL.DbModels
{
    public class EventResult
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string BattleResults { get; set; }
        public bool? IsLost { get; set; }
        public DateTime EventDate { get; set; }
        public virtual Event Event { get; set; }
    }
}
