﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using WgAppService.DAL.DbModels.Dictionaries;

namespace WgAppService.DAL.DbModels
{
    public class Event
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public Guid EventUuid { get; set; }
        public string Name { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime LastUpdated { get; set; }
        public bool IsActive { get; set; }
        public virtual RepeatType RepeatType { get; set; }
        public virtual Participant EventCreator { get; set; }
        public virtual EventType EventType { get; set; }
        public virtual BattleType BattleType { get; set; }
        public virtual Map Map { get; set; }

        public Event()
        {
            CreateDate = DateTime.UtcNow;
            LastUpdated = DateTime.UtcNow;
            IsActive = true;
        }
    }
}
