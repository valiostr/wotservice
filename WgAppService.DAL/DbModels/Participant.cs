﻿using System.ComponentModel.DataAnnotations.Schema;

namespace WgAppService.DAL.DbModels
{
    public class Participant
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public long PlayerUuid { get; set; }
        public string Name { get; set; }
        public string DeviceId { get; set; }
    }
}
