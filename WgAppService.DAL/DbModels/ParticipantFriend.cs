﻿using System.ComponentModel.DataAnnotations.Schema;

namespace WgAppService.DAL.DbModels
{
    public class ParticipantFriend
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public virtual Participant Participant { get; set; }
        public virtual Participant Friend { get; set; }
    }
}
