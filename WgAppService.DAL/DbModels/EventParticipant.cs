﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using WgAppService.DAL.DbModels.Dictionaries;

namespace WgAppService.DAL.DbModels
{
    public class EventParticipant
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public DateTime? AnswerTime { get; set; }
        public bool? IsInvited { get; set; }
        public bool? IsCommander { get; set; }
        public virtual Participant Participant { get; set; }
        public virtual Event Event { get; set; }
        public virtual PlayerRole Role { get; set; }
        public virtual UserAnswer UserAnswerType { get; set; }
    }
}
