﻿using System.IO;
using System.Net;
using System.Text;
using WgAppService.DAL.Interfaces;
using WgAppService.Utilities;
using WgAppService.Utilities.Enumerations;

namespace WgAppService.DAL
{
    public class WotApiRequestManager : IWotApiRequestManager
    {
        public string GetWgBaseUserData(string rowJson)
        {
            return SendPostRequest(WgAppServiceHelper.GenerateWotUrl(RequestType.BaseUserData), rowJson);
        }
        public string GetClanInfo(string rowJson)
        {
            return SendPostRequest(WgAppServiceHelper.GenerateWotUrl(RequestType.ClanData), rowJson);
        }

        public string GetClanEvents(string rowJson)
        {
            return SendPostRequest(WgAppServiceHelper.GenerateWotUrl(RequestType.ClanEvents), rowJson);
        }

        public string GetProvincesInfo(string rowJson)
        {
            return SendPostRequest(WgAppServiceHelper.GenerateWotUrl(RequestType.ProvinceInfo), rowJson);
        }

        public string GetWgTanksData(string rowJson)
        {
            return SendPostRequest(WgAppServiceHelper.GenerateWotUrl(RequestType.TankData), rowJson);
        }
        public string GetWgTanksStatistics(string rowJson)
        {
            return SendPostRequest(WgAppServiceHelper.GenerateWotUrl(RequestType.TankStatistics), rowJson);
        }

        private string SendPostRequest(string url, string postJson)
        {
            var wgUserDataRequest = (HttpWebRequest)WebRequest.Create(url);
            wgUserDataRequest.ContentType = "application/x-www-form-urlencoded";
            wgUserDataRequest.Method = "POST";
            var requestStream = wgUserDataRequest.GetRequestStream();
            var bytesArray = Encoding.UTF8.GetBytes(postJson);
            requestStream.Write(bytesArray, 0, bytesArray.Length);
            requestStream.Close();
            try
            {
                using (var wgUserDataResponse = wgUserDataRequest.GetResponse() as HttpWebResponse)
                {
                    if (wgUserDataRequest.HaveResponse && wgUserDataResponse != null)
                    {
                        using (var reader = new StreamReader(wgUserDataResponse.GetResponseStream()))
                        {
                            return reader.ReadToEnd();
                        }
                    }

                }
                return string.Empty;
            }
            catch (WebException ex)
            {
                throw new WebException(ex.Message);
            }
        }


       
    }
}
