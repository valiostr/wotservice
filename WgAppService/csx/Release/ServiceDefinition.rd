﻿<?xml version="1.0" encoding="utf-8"?>
<serviceModel xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" name="WgAppService" generation="1" functional="0" release="0" Id="513e5a48-35eb-4e83-9edd-9ec274bf1697" dslVersion="1.2.0.0" xmlns="http://schemas.microsoft.com/dsltools/RDSM">
  <groups>
    <group name="WgAppServiceGroup" generation="1" functional="0" release="0">
      <componentports>
        <inPort name="WgAppServiceRole:WgServiceEndpoint" protocol="http">
          <inToChannel>
            <lBChannelMoniker name="/WgAppService/WgAppServiceGroup/LB:WgAppServiceRole:WgServiceEndpoint" />
          </inToChannel>
        </inPort>
      </componentports>
      <settings>
        <aCS name="WgAppServiceRole:Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" defaultValue="">
          <maps>
            <mapMoniker name="/WgAppService/WgAppServiceGroup/MapWgAppServiceRole:Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" />
          </maps>
        </aCS>
        <aCS name="WgAppServiceRoleInstances" defaultValue="[1,1,1]">
          <maps>
            <mapMoniker name="/WgAppService/WgAppServiceGroup/MapWgAppServiceRoleInstances" />
          </maps>
        </aCS>
      </settings>
      <channels>
        <lBChannel name="LB:WgAppServiceRole:WgServiceEndpoint">
          <toPorts>
            <inPortMoniker name="/WgAppService/WgAppServiceGroup/WgAppServiceRole/WgServiceEndpoint" />
          </toPorts>
        </lBChannel>
      </channels>
      <maps>
        <map name="MapWgAppServiceRole:Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" kind="Identity">
          <setting>
            <aCSMoniker name="/WgAppService/WgAppServiceGroup/WgAppServiceRole/Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" />
          </setting>
        </map>
        <map name="MapWgAppServiceRoleInstances" kind="Identity">
          <setting>
            <sCSPolicyIDMoniker name="/WgAppService/WgAppServiceGroup/WgAppServiceRoleInstances" />
          </setting>
        </map>
      </maps>
      <components>
        <groupHascomponents>
          <role name="WgAppServiceRole" generation="1" functional="0" release="0" software="D:\Projects\WgApp\WgAppService\csx\Release\roles\WgAppServiceRole" entryPoint="base\x64\WaHostBootstrapper.exe" parameters="base\x64\WaIISHost.exe " memIndex="1792" hostingEnvironment="frontendadmin" hostingEnvironmentVersion="2">
            <componentports>
              <inPort name="WgServiceEndpoint" protocol="http" portRanges="80" />
            </componentports>
            <settings>
              <aCS name="Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" defaultValue="" />
              <aCS name="__ModelData" defaultValue="&lt;m role=&quot;WgAppServiceRole&quot; xmlns=&quot;urn:azure:m:v1&quot;&gt;&lt;r name=&quot;WgAppServiceRole&quot;&gt;&lt;e name=&quot;WgServiceEndpoint&quot; /&gt;&lt;/r&gt;&lt;/m&gt;" />
            </settings>
            <resourcereferences>
              <resourceReference name="DiagnosticStore" defaultAmount="[4096,4096,4096]" defaultSticky="true" kind="Directory" />
              <resourceReference name="WgAppServiceRole.svclog" defaultAmount="[1000,1000,1000]" defaultSticky="true" kind="Directory" />
              <resourceReference name="EventStore" defaultAmount="[1000,1000,1000]" defaultSticky="false" kind="LogStore" />
            </resourcereferences>
          </role>
          <sCSPolicy>
            <sCSPolicyIDMoniker name="/WgAppService/WgAppServiceGroup/WgAppServiceRoleInstances" />
            <sCSPolicyUpdateDomainMoniker name="/WgAppService/WgAppServiceGroup/WgAppServiceRoleUpgradeDomains" />
            <sCSPolicyFaultDomainMoniker name="/WgAppService/WgAppServiceGroup/WgAppServiceRoleFaultDomains" />
          </sCSPolicy>
        </groupHascomponents>
      </components>
      <sCSPolicy>
        <sCSPolicyUpdateDomain name="WgAppServiceRoleUpgradeDomains" defaultPolicy="[5,5,5]" />
        <sCSPolicyFaultDomain name="WgAppServiceRoleFaultDomains" defaultPolicy="[2,2,2]" />
        <sCSPolicyID name="WgAppServiceRoleInstances" defaultPolicy="[1,1,1]" />
      </sCSPolicy>
    </group>
  </groups>
  <implements>
    <implementation Id="a5fa3d40-0c5b-4d7a-842d-17ed83b1c91c" ref="Microsoft.RedDog.Contract\ServiceContract\WgAppServiceContract@ServiceDefinition">
      <interfacereferences>
        <interfaceReference Id="a942a77f-3885-43f5-b76e-08a0b5c14145" ref="Microsoft.RedDog.Contract\Interface\WgAppServiceRole:WgServiceEndpoint@ServiceDefinition">
          <inPort>
            <inPortMoniker name="/WgAppService/WgAppServiceGroup/WgAppServiceRole:WgServiceEndpoint" />
          </inPort>
        </interfaceReference>
      </interfacereferences>
    </implementation>
  </implements>
</serviceModel>