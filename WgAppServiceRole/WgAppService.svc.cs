﻿using System;
using System.Collections.Generic;
using AutoMapper;
using WgAppService.BL;
using WgAppService.Contracts;
using WgAppService.Contracts.DataContracts.UserInfo;
using WgAppService.Contracts.Messages;
using WgAppService.DAL.Models;
using WgAppService.Contracts.DataContracts;
using WgAppService.DAL.Models.ApiModels;

namespace WgAppServiceRole
{
    public class WgAppService : WgAppServiceBase, IWgAppService
    {
        private readonly WotRequests _wotRequests;
        public WgAppService()
        {
            _wotRequests = new WotRequests();
            InitMapper();
        }
        #region Mapper
        protected void InitMapper()
        {
            Mapper.CreateMap<UserInfo, UserInfoDto>()
                .ForMember(s => s.AccountId, d => d.MapFrom(dest => dest.account_id))
                .ForMember(s => s.Achievements, d => d.MapFrom(dest => dest.achievements))
                .ForMember(s => s.ClanId, d => d.MapFrom(dest => dest.clan_id))
                .ForMember(s => s.ClientLanguage, d => d.MapFrom(dest => dest.client_language))
                .ForMember(s => s.CreatedAt, d => d.MapFrom(dest => dest.created_at))
                .ForMember(s => s.GlobalRating, d => d.MapFrom(dest => dest.global_rating))
                .ForMember(s => s.LastBattleTime, d => d.MapFrom(dest => dest.last_battle_time))
                .ForMember(s => s.LogoutAt, d => d.MapFrom(dest => dest.logout_at))
                .ForMember(s => s.Nickname, d => d.MapFrom(dest => dest.nickname))
                .ForMember(s => s.Private, d => d.MapFrom(dest => dest.@private))
                .ForMember(s => s.Statistics, d => d.MapFrom(dest => dest.statistics))
                .ForMember(s => s.UpdatedAt, d => d.MapFrom(dest => dest.updated_at));

            Mapper.CreateMap<PrivateInfo, PrivateInfoDto>()
                .ForMember(s => s.AccountType, d => d.MapFrom(dest => dest.account_type))
                .ForMember(s => s.AccountTypeI18N, d => d.MapFrom(dest => dest.account_type_i18n))
                .ForMember(s => s.Credits, d => d.MapFrom(dest => dest.credits))
                .ForMember(s => s.FreeXp, d => d.MapFrom(dest => dest.free_xp))
                .ForMember(s => s.Friends, d => d.MapFrom(dest => dest.friends))
                .ForMember(s => s.Gold, d => d.MapFrom(dest => dest.gold))
                .ForMember(s => s.IsBoundToPhone, d => d.MapFrom(dest => dest.is_bound_to_phone))
                .ForMember(s => s.IsPremium, d => d.MapFrom(dest => dest.is_premium))
                .ForMember(s => s.PremiumExpiresAt, d => d.MapFrom(dest => dest.premium_expires_at));

            Mapper.CreateMap<Statistics, StatisticsDto>()
                .ForMember(s => s.All, d => d.MapFrom(dest => dest.all))
                .ForMember(s => s.Clan, d => d.MapFrom(dest => dest.clan))
                .ForMember(s => s.Company, d => d.MapFrom(dest => dest.company))
                .ForMember(s => s.Historical, d => d.MapFrom(dest => dest.historical))
                .ForMember(s => s.MaxDamage, d => d.MapFrom(dest => dest.max_damage))
                .ForMember(s => s.MaxDamageVehicle, d => d.MapFrom(dest => dest.max_damage_vehicle))
                .ForMember(s => s.MaxXp, d => d.MapFrom(dest => dest.max_xp));

            Mapper.CreateMap<StatisticsBase, StatisticsBaseDto>()
                .ForMember(s => s.BattleAvgXp, d => d.MapFrom(dest => dest.battle_avg_xp))
                .ForMember(s => s.Battles, d => d.MapFrom(dest => dest.battles))
                .ForMember(s => s.CapturePoints, d => d.MapFrom(dest => dest.capture_points))
                .ForMember(s => s.DamageDealt, d => d.MapFrom(dest => dest.damage_dealt))
                .ForMember(s => s.DamageReceived, d => d.MapFrom(dest => dest.damage_received))
                .ForMember(s => s.Draws, d => d.MapFrom(dest => dest.draws))
                .ForMember(s => s.DroppedCapturePoints, d => d.MapFrom(dest => dest.dropped_capture_points))
                .ForMember(s => s.Frags, d => d.MapFrom(dest => dest.frags))
                .ForMember(s => s.Hits, d => d.MapFrom(dest => dest.hits))
                .ForMember(s => s.HitsPercents, d => d.MapFrom(dest => dest.hits_percents))
                .ForMember(s => s.Losses, d => d.MapFrom(dest => dest.losses))
                .ForMember(s => s.Shots, d => d.MapFrom(dest => dest.shots))
                .ForMember(s => s.Spotted, d => d.MapFrom(dest => dest.spotted))
                .ForMember(s => s.SurvivedBattles, d => d.MapFrom(dest => dest.survived_battles))
                .ForMember(s => s.Wins, d => d.MapFrom(dest => dest.shots))
                .ForMember(s => s.Xp, d => d.MapFrom(dest => dest.spotted));

            Mapper.CreateMap<Achievements, AchievementsDto>()
                .ForMember(s => s.ArmorPiercer, d => d.MapFrom(dest => dest.armor_piercer))
                .ForMember(s => s.Beasthunter, d => d.MapFrom(dest => dest.beasthunter))
                .ForMember(s => s.Bombardier, d => d.MapFrom(dest => dest.bombardier))
                .ForMember(s => s.Defender, d => d.MapFrom(dest => dest.defender))
                .ForMember(s => s.Diehard, d => d.MapFrom(dest => dest.diehard))
                .ForMember(s => s.Evileye, d => d.MapFrom(dest => dest.evileye))
                .ForMember(s => s.HandOfDeath, d => d.MapFrom(dest => dest.hand_of_death))
                .ForMember(s => s.Huntsman, d => d.MapFrom(dest => dest.huntsman))
                .ForMember(s => s.Invader, d => d.MapFrom(dest => dest.invader))
                .ForMember(s => s.Invincible, d => d.MapFrom(dest => dest.invincible))
                .ForMember(s => s.IronMan, d => d.MapFrom(dest => dest.iron_man))
                .ForMember(s => s.Kamikaze, d => d.MapFrom(dest => dest.kamikaze))
                .ForMember(s => s.LuckyDevil, d => d.MapFrom(dest => dest.lucky_devil))
                .ForMember(s => s.Lumberjack, d => d.MapFrom(dest => dest.lumberjack))
                .ForMember(s => s.MainGun, d => d.MapFrom(dest => dest.main_gun))
                .ForMember(s => s.MaxDiehardSeries, d => d.MapFrom(dest => dest.max_diehard_series))
                .ForMember(s => s.MaxInvincibleSeries, d => d.MapFrom(dest => dest.max_invincible_series))
                .ForMember(s => s.MaxKillingSeries, d => d.MapFrom(dest => dest.max_killing_series))
                .ForMember(s => s.MaxPiercingSeries, d => d.MapFrom(dest => dest.max_piercing_series))
                .ForMember(s => s.MaxSniperSeries, d => d.MapFrom(dest => dest.max_sniper_series))
                .ForMember(s => s.MechanicEngineer, d => d.MapFrom(dest => dest.mechanic_engineer))
                .ForMember(s => s.MechanicEngineerChina, d => d.MapFrom(dest => dest.mechanic_engineer_china))
                .ForMember(s => s.MechanicEngineerFrance, d => d.MapFrom(dest => dest.mechanic_engineer_france))
                .ForMember(s => s.MechanicEngineerGermany, d => d.MapFrom(dest => dest.mechanic_engineer_germany))
                .ForMember(s => s.MechanicEngineerJapan, d => d.MapFrom(dest => dest.mechanic_engineer_japan))
                .ForMember(s => s.MechanicEngineerUk, d => d.MapFrom(dest => dest.mechanic_engineer_uk))
                .ForMember(s => s.MechanicEngineerUsa, d => d.MapFrom(dest => dest.mechanic_engineer_usa))
                .ForMember(s => s.MechanicEngineerUssr, d => d.MapFrom(dest => dest.mechanic_engineer_ussr))
                .ForMember(s => s.MedalAbrams, d => d.MapFrom(dest => dest.medal_abrams))
                .ForMember(s => s.MedalBillotte, d => d.MapFrom(dest => dest.medal_billotte))
                .ForMember(s => s.MedalBoelter, d => d.MapFrom(dest => dest.medal_boelter))
                .ForMember(s => s.MedalBrothersInArms, d => d.MapFrom(dest => dest.medal_brothers_in_arms))
                .ForMember(s => s.MedalBrunoPietro, d => d.MapFrom(dest => dest.medal_bruno_pietro))
                .ForMember(s => s.MedalBurda, d => d.MapFrom(dest => dest.medal_burda))
                .ForMember(s => s.MedalCarius, d => d.MapFrom(dest => dest.medal_carius))
                .ForMember(s => s.MedalCrucialContribution, d => d.MapFrom(dest => dest.medal_crucial_contribution))
                .ForMember(s => s.MedalDelanglade, d => d.MapFrom(dest => dest.medal_delanglade))
                .ForMember(s => s.MedalDumitru, d => d.MapFrom(dest => dest.medal_dumitru))
                .ForMember(s => s.MedalEkins, d => d.MapFrom(dest => dest.medal_ekins))
                .ForMember(s => s.MedalFadin, d => d.MapFrom(dest => dest.medal_fadin))
                .ForMember(s => s.MedalHalonen, d => d.MapFrom(dest => dest.medal_halonen))
                .ForMember(s => s.MedalHeroesOfRassenay, d => d.MapFrom(dest => dest.medal_heroes_of_rassenay))
                .ForMember(s => s.MedalKay, d => d.MapFrom(dest => dest.medal_kay))
                .ForMember(s => s.MedalKnispel, d => d.MapFrom(dest => dest.medal_knispel))
                .ForMember(s => s.MedalKolobanov, d => d.MapFrom(dest => dest.medal_kolobanov))
                .ForMember(s => s.MedalLafayettePool, d => d.MapFrom(dest => dest.medal_lafayette_pool))
                .ForMember(s => s.MedalLavrinenko, d => d.MapFrom(dest => dest.medal_lavrinenko))
                .ForMember(s => s.MedalLeClerc, d => d.MapFrom(dest => dest.medal_le_clerc))
                .ForMember(s => s.MedalLehvaslaiho, d => d.MapFrom(dest => dest.medal_lehvaslaiho))
                .ForMember(s => s.MedalNikolas, d => d.MapFrom(dest => dest.medal_nikolas))
                .ForMember(s => s.MedalOrlik, d => d.MapFrom(dest => dest.medal_orlik))
                .ForMember(s => s.MedalOskin, d => d.MapFrom(dest => dest.medal_oskin))
                .ForMember(s => s.MedalPascucci, d => d.MapFrom(dest => dest.medal_pascucci))
                .ForMember(s => s.MedalPoppel, d => d.MapFrom(dest => dest.medal_poppel))
                .ForMember(s => s.MedalRadleyWalters, d => d.MapFrom(dest => dest.medal_radley_walters))
                .ForMember(s => s.MedalTamadaYoshio, d => d.MapFrom(dest => dest.medal_tamada_yoshio))
                .ForMember(s => s.MedalTarczay, d => d.MapFrom(dest => dest.medal_tarczay))
                .ForMember(s => s.Mousebane, d => d.MapFrom(dest => dest.mousebane))
                .ForMember(s => s.PattonValley, d => d.MapFrom(dest => dest.patton_valley))
                .ForMember(s => s.Raider, d => d.MapFrom(dest => dest.raider))
                .ForMember(s => s.Scout, d => d.MapFrom(dest => dest.scout))
                .ForMember(s => s.Sinai, d => d.MapFrom(dest => dest.sinai))
                .ForMember(s => s.Sniper, d => d.MapFrom(dest => dest.sniper))
                .ForMember(s => s.Sniper2, d => d.MapFrom(dest => dest.sniper2))
                .ForMember(s => s.Steelwall, d => d.MapFrom(dest => dest.steelwall))
                .ForMember(s => s.Sturdy, d => d.MapFrom(dest => dest.sturdy))
                .ForMember(s => s.Supporter, d => d.MapFrom(dest => dest.supporter))
                .ForMember(s => s.TankExpert, d => d.MapFrom(dest => dest.tank_expert))
                .ForMember(s => s.TankExpertChina, d => d.MapFrom(dest => dest.tank_expert_china))
                .ForMember(s => s.TankExpertFrance, d => d.MapFrom(dest => dest.tank_expert_france))
                .ForMember(s => s.TankExpertGermany, d => d.MapFrom(dest => dest.tank_expert_germany))
                .ForMember(s => s.TankExpertJapan, d => d.MapFrom(dest => dest.tank_expert_japan))
                .ForMember(s => s.TankExpertUk, d => d.MapFrom(dest => dest.tank_expert_uk))
                .ForMember(s => s.TankExpertUsa, d => d.MapFrom(dest => dest.tank_expert_usa))
                .ForMember(s => s.TankExpertUssr, d => d.MapFrom(dest => dest.tank_expert_ussr))
                .ForMember(s => s.TitleSniper, d => d.MapFrom(dest => dest.title_sniper))
                .ForMember(s => s.Warrior, d => d.MapFrom(dest => dest.warrior));
            Mapper.CreateMap<BaseUserData, BaseUserInfo>()
                .ForMember(s => s.LastBattleTime, d => d.MapFrom(dest => dest.last_battle_time))
                .ForMember(s => s.Nickname, d => d.MapFrom(dest => dest.nickname))
                .ForMember(s => s.UpdatedAt, d => d.MapFrom(dest => dest.updated_at))
                .ForMember(s => s.UserId, d => d.MapFrom(dest => dest.account_id));
            Mapper.CreateMap<ParticipantInfoDto, ParticipantInfo>();
            Mapper.CreateMap<ParticipantModel, ParticipantDto>();
            Mapper.CreateMap<EventModel, EventInfoDto>();
            Mapper.CreateMap<SyncModel, SyncDataResponse>();
            Mapper.CreateMap<MapModel, MapInfoDto>();
            Mapper.CreateMap<CreateEventData, CreateEventModel>();
            Mapper.CreateMap<BestTankModel, BestTankDto>();
        }

        #endregion
        public UserInfoResponse SyncUserData(BaseSyncData data)
        {
            var response = new UserInfoResponse();
            try
            {
                response.BasicUserInfo = Mapper.Map<List<BaseUserData>, List<BaseUserInfo>>(_wotRequests.SyncUserData(data.UserId, data.FriendsIds, data.TimeStamp, data.DeviceId, data.Tanks));
                SetSuccessInfo(response);
            }
            catch (Exception ex)
            {
                SetErrorInfo(ex, response);
            }

            return response;
        }
        public SyncDataResponse IncrementSync(IncrementSyncData data)
        {
            var response = new SyncDataResponse();
            try
            {
                response = Mapper.Map<SyncModel, SyncDataResponse>(_wotRequests.IncrementSync(data.UserId, data.Timestamp, data.ClanId, data.MapId));
                SetSuccessInfo(response);
            }
            catch (Exception ex)
            {
                SetErrorInfo(ex, response);
            }

            return response;
        }
        public CreateEventResponse CreateEvent(CreateEventData data)
        {
            var response = new CreateEventResponse();
            try
            {
                response.EventId = _wotRequests.CreateEvent(Mapper.Map<CreateEventData, CreateEventModel>(data));
                SetSuccessInfo(response);
            }
            catch (Exception ex)
            {
                SetErrorInfo(ex, response);
            }

            return response;
        }
        public ResponseBase UpdateEvent(CreateEventData data)
        {
            var response = new ResponseBase();
            try
            {
                _wotRequests.UpdateEvent(Mapper.Map<CreateEventData, CreateEventModel>(data));
                SetSuccessInfo(response);
            }
            catch (Exception ex)
            {
                SetErrorInfo(ex, response);
            }

            return response;
        }
        public ResponseBase DropEvents(DropEventData data)
        {
            var response = new ResponseBase();
            try
            {
                _wotRequests.DropEvents(data.EventId, data.UserId);
                SetSuccessInfo(response);
            }
            catch (Exception ex)
            {
                SetErrorInfo(ex, response);
            }

            return response;
        }
        public ResponseBase SetUserAnswer(UserAnswerData data)
        {
            var response = new ResponseBase();
            try
            {
                _wotRequests.SetUserAnswer(data.UserId, data.EventId, data.Answer);
                SetSuccessInfo(response);
            }
            catch (Exception ex)
            {
                SetErrorInfo(ex, response);
            }

            return response;
        }
        public BestTanksResponce GetBestTanks(BaseSyncData data)
        {
            var response = new BestTanksResponce();
            try
            {
                response.BestTanks = Mapper.Map<List<BestTankModel>,List<BestTankDto>>(_wotRequests.GetBestTanks(data.UserId));
                SetSuccessInfo(response);
            }
            catch (Exception ex)
            {
                SetErrorInfo(ex, response);
            }

            return response;
        }
    }
}
