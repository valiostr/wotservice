﻿using System;
using System.Configuration;
using System.ServiceModel;
using System.ServiceModel.Activation;
using WgAppService.Contracts.Messages;
using WgAppService.Utilities;
using WgAppService.Utilities.Enumerations;

namespace WgAppServiceRole
{
    [ServiceBehavior(IncludeExceptionDetailInFaults = true, InstanceContextMode = InstanceContextMode.Single, ConcurrencyMode = ConcurrencyMode.Single)]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]  
    public class WgAppServiceBase
    {
        protected void SetErrorInfo(Exception e, ResponseBase response)
        {
            response.ResponseStatus = ResponseStatus.Failed;
            response.Message = e.Message;
            response.Build = ConfigurationManager.AppSettings["Build"];
            response.Version = ConfigurationManager.AppSettings["Version"];
            response.TimeStamp = WgAppServiceHelper.GetTimeStamp();
        }

        protected void SetSuccessInfo(ResponseBase response)
        {
            response.ResponseStatus = ResponseStatus.Success;
            response.Build = ConfigurationManager.AppSettings["Build"];
            response.Version = ConfigurationManager.AppSettings["Version"];
            response.Message = ResponseStatus.Success.ToString();
            response.TimeStamp = WgAppServiceHelper.GetTimeStamp();
        }
    }
}