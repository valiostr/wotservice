﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using WgAppService.DAL;
using WgAppService.DAL.DbModels;
using WgAppService.DAL.Interfaces;
using WgAppService.DAL.Models;
using WgAppService.DAL.Models.ApiModels;
using WgAppService.DAL.Repositories;
using WgAppService.Utilities;
using WgAppService.Utilities.Enumerations;

namespace WgAppService.BL
{
    public class WotRequests
    {
        private readonly IWotApiRequestManager _wotApiManager;
        private readonly IWgDbRepository _repository;
        private const string MembersKey = "members";
        private readonly NotificationsHelper _notifications;

        public WotRequests()
        {
            _wotApiManager = new WotApiRequestManager();
            _repository = new WgDbRepository();
            _notifications = new NotificationsHelper();
        }
        public List<BaseUserData> SyncUserData(long accountId, List<long> friends, int timestamp, string deviceId, List<long> tanks)
        {
            if (accountId <= 0)
            {
                ExceptionsHelper.RiseError("Account Id (value:{0}) is not specified", accountId.ToString(CultureInfo.InvariantCulture));
            }

            if (!IsParticipantInList(accountId, friends))
            {
                friends.Add(accountId);
            }
            var resultObject = GetUsersData(friends.ToArray());
            SetAvgDamageAndWinRate(resultObject);
            Task.Run(() =>
            {
                var usr = resultObject.FirstOrDefault(p => p.account_id == accountId);
                if (usr != null)
                {
                    _repository.CreateParticipant(usr.nickname, usr.account_id, deviceId);
                }
                _repository.CreateParticipants(resultObject);
                _repository.SetUserFriends(accountId, friends);
                SetBestTanksInfo(accountId, tanks);
            });
            return resultObject.Where(t => t != null && t.updated_at > timestamp && t.account_id != accountId).ToList();
        }
        public SyncModel IncrementSync(long accountId, int timestamp, long clanId, string mapId)
        {
            if (accountId <= 0)
            {
                ExceptionsHelper.RiseError("Account Id (value:{0}) is not specified", accountId.ToString(CultureInfo.InvariantCulture));
            }

            var result = new SyncModel();
            var events = GetEvents(accountId, false, timestamp);
            if (clanId != 0 && !string.IsNullOrEmpty(mapId))
            {
                var clanEvents = GetClanEvents(clanId, mapId);
                if (clanEvents.Count > 0)
                {
                    var provinces = GetProvinceInfo(clanEvents.Select(s => s.provinces[0]).ToList(), mapId);
                    foreach (var clanEvent in clanEvents)
                    {
                        var prov = provinces.SingleOrDefault(p => p.province_id == clanEvent.provinces[0]);
                        if (prov != null)
                        {
                            clanEvent.ProvinceInfo.Add(prov);
                        }
                    }
                    var clanInfo = GetClanInfo(clanId);
                    CreateParticipantsIfNotExists(new[] { clanInfo.owner_id });
                    foreach (var mems in clanInfo.ClanMembers)
                    {
                        result.ClanMembersData.Add(new BaseUserData { account_id = mems.account_id, nickname = mems.account_name, last_battle_time = 0, updated_at = 0 });
                    }
                    var newEvs = _repository.CreateEvent(clanEvents, clanInfo.owner_id);
                    foreach (var @event in newEvs)
                    {
                        if (events.All(ev => ev.Name == @event.Name))
                        {
                            var ev = new EventModel
                            {
                                EventId = @event.EventUuid,
                                Name = @event.Name,
                                EventType = @event.EventType.Code,
                                BattleType = @event.BattleType.Code,
                                EnableToEdit = accountId == @event.EventCreator.PlayerUuid,
                                EndDate = WgAppServiceHelper.GetTimeStamp(@event.EndDate),
                                StartDate = WgAppServiceHelper.GetTimeStamp(@event.StartDate),
                                Map = new MapModel { EnName = @event.Map.EnName, MapCode = @event.Map.Code, RuName = @event.Map.RuName },
                                LastUpdated = WgAppServiceHelper.GetTimeStamp(@event.LastUpdated),
                                RepeatType = (int)RepeateTypes.NoRepeat
                            };
                            var owner = _repository.GetParticipant(@event.EventCreator.Id);
                            if (owner != null)
                            {
                                ev.Owner = new ParticipantModel
                                {
                                    UserId = owner.PlayerUuid,
                                    Name = owner.Name,
                                    PlayerRole = (int)RoleTypes.Main,
                                };
                            }
                            events.Add(ev);
                        }
                    }
                }
            }
            if (events.Count <= 0)
                return result;

            result.EventClanWarsData = events.Where(e => e.EventType == (int)EventTypes.ClanWar).ToList();
            result.EventPlatoonsData = events.Where(e => e.EventType == (int)EventTypes.Plattoon).ToList();
            result.EventCompanysData = events.Where(e => e.EventType == (int)EventTypes.Company).ToList();

            return result;
        }
        public Guid CreateEvent(CreateEventModel eventModel)
        {
            if (eventModel.UserId <= 0 || string.IsNullOrEmpty(eventModel.Name) || string.IsNullOrEmpty(eventModel.StartDate) || string.IsNullOrEmpty(eventModel.EndDate))
            {
                ExceptionsHelper.RiseError("UserId, event Name, event startDate and Event EndDate are required current values are: UserId:{0},event Name{1},startDate{2},EndDate{3}", new[] { eventModel.UserId.ToString(CultureInfo.InvariantCulture), eventModel.Name, eventModel.StartDate, eventModel.EndDate });
            }
            if (DateTime.Parse(eventModel.StartDate) < DateTime.Now || DateTime.Parse(eventModel.StartDate) < DateTime.Parse(eventModel.EndDate))
            {
                ExceptionsHelper.RiseError("Start Date must be greater then End Date and Start date must be greater or equals current date. Values: SD: {0}, ED: {1}", new[] { eventModel.StartDate, eventModel.EndDate });
            }

            if (!IsParticipantInList(eventModel.UserId, eventModel.Participants))
            {
                eventModel.Participants.Add(new ParticipantInfo { ParticipantUuid = eventModel.UserId, Role = 0 });
            }
            long[] friends = eventModel.Participants.Select(t => t.ParticipantUuid).ToArray();
            CreateParticipantsIfNotExists(friends);
            eventModel.EventId = Guid.NewGuid();
            Task.Run(() =>
            {
                _repository.CreateEvent(eventModel);
                var parts = _repository.GetParticipants(friends.ToList());
                string body = JsonSerializer.GenerateJsonBody(new PushModel(string.Format("You invited to take part in '{0}' event", eventModel.Name), eventModel.StartDate));
                foreach (Participant participant in parts.Where(participant => !string.IsNullOrEmpty(participant.DeviceId)))
                {
                    _notifications.SendPush(participant.DeviceId, body);
                }
            });

            return eventModel.EventId;
        }
        public void UpdateEvent(CreateEventModel eventModel)
        {
            if (!_repository.EventExists(eventModel.EventId))
            {
                ExceptionsHelper.RiseError("No Event {0} does not exists", eventModel.EventId.ToString());
            }
            long[] friends = eventModel.Participants != null && eventModel.Participants.Count > 0 ? eventModel.Participants.Select(t => t.ParticipantUuid).ToArray() : new long[0];
            CreateParticipantsIfNotExists(friends);
            if (eventModel.Participants == null || eventModel.Participants.Count == 0)
            {
                Task.Run(() => _repository.UpdateEvent(eventModel));
            }
        }
        public List<EventModel> GetEvents(long ownerUuid, bool onlyActive, int timestamp)
        {
            var result = new List<EventModel>();
            if (ownerUuid > 0)
            {
                var events = _repository.GetEvents(ownerUuid, onlyActive);
                foreach (var @event in events)
                {
                    var ev = new EventModel
                    {
                        EndDate = WgAppServiceHelper.GetTimeStamp(@event.EndDate),
                        EventId = @event.EventUuid,
                        Name = @event.Name,
                        RepeatType = @event.RepeatType.Code,
                        StartDate = WgAppServiceHelper.GetTimeStamp(@event.StartDate),
                        LastUpdated = WgAppServiceHelper.GetTimeStamp(@event.LastUpdated),
                        EventType = @event.EventType.Code,
                        BattleType = @event.BattleType != null ? @event.BattleType.Code : (int?)null,
                        EnableToEdit = true
                    };
                    if (@event.Map != null)
                    {
                        ev.Map = new MapModel
                        {
                            MapCode = @event.Map.Code,
                            EnName = @event.Map.EnName,
                            RuName = @event.Map.RuName
                        };
                    }
                    if (@event.EventType.Code == (int)EventTypes.ClanWar)
                    {
                        var results = _repository.GetEventResult(@event.Id, @event.StartDate);
                        if (results != null)
                        {
                            ev.Results = results.BattleResults;
                        }
                    }
                    var participants = _repository.GetEventParticipantsByEvent(@event.EventUuid).ToList();
                    var owner = participants.SingleOrDefault(p => p.Participant.PlayerUuid == ownerUuid && p.Event.EventUuid == ev.EventId);
                    if (owner != null)
                    {
                        ev.Owner = new ParticipantModel
                        {
                            UserId = owner.Participant.PlayerUuid,
                            Name = owner.Participant.Name,
                            PlayerRole = owner.Role != null ? owner.Role.Code : (int)RoleTypes.Main,
                            AnswerTime = owner.AnswerTime != null ? WgAppServiceHelper.GetTimeStamp(owner.AnswerTime.Value) : (int?)null,
                            UserAnswer = owner.UserAnswerType != null ? owner.UserAnswerType.Code : (int?)null
                        };
                    }
                    else
                    {
                        ev.Owner = new ParticipantModel
                        {
                            IsCommander = true,
                            Name = @event.EventCreator.Name,
                            PlayerRole = (int)RoleTypes.Main,
                            UserId = @event.EventCreator.PlayerUuid
                        };
                    }
                    foreach (var partModel in participants.Select(eventParticipant => new ParticipantModel
                    {
                        UserId = eventParticipant.Participant.PlayerUuid,
                        Name = eventParticipant.Participant.Name,
                        PlayerRole = eventParticipant.Role != null ? eventParticipant.Role.Code : (int)RoleTypes.Secondary,
                        AnswerTime = eventParticipant.AnswerTime != null ? WgAppServiceHelper.GetTimeStamp(eventParticipant.AnswerTime.Value) : (int?)null,
                        UserAnswer = eventParticipant.UserAnswerType != null ? eventParticipant.UserAnswerType.Code : (int?)null
                    }).Where(p => p.UserId != ownerUuid))
                    {
                        ev.Participants.Add(partModel);
                    }
                    ev.ExtraRepeatData = _repository.GetRepeatDataByEvent(@event.EventUuid).Select(c => c.Code).ToList();
                    result.Add(ev);
                }
            }
            else
            {
                ExceptionsHelper.RiseError("User with Id {0} not found", ownerUuid.ToString(CultureInfo.InvariantCulture));
            }
            return timestamp > 0 ? result.Where(evs => evs.LastUpdated > timestamp).ToList() : result;
        }
        public void DropEvents(Guid eventId, long userId)
        {
            if (eventId != Guid.Empty || userId > 0)
            {
                if (userId > 0)
                {
                    Task.Run(() => _repository.DropEventsForUser(userId));
                }
                else
                {
                    Task.Run(() => _repository.DropEvent(eventId));
                }
            }
            else
            {
                ExceptionsHelper.RiseError("User Id or event id must been specified. Current values: Uid{0}, EvId{1}", new[] { userId.ToString(CultureInfo.InvariantCulture), eventId.ToString() });
            }
        }
        public void SetUserAnswer(long userUuid, Guid eventUuid, int answer)
        {
            if (userUuid > 0 && eventUuid != Guid.Empty)
            {
                Task.Run(() =>
                {
                    _repository.SetUserAnswer(userUuid, eventUuid, answer);
                    var ev = _repository.GetEvent(eventUuid);
                    if (ev.EventCreator !=null && !string.IsNullOrEmpty(ev.EventCreator.DeviceId))
                    {
                        var part = _repository.GetParticipant(userUuid);
                        string body = JsonSerializer.GenerateJsonBody(new PushModel(string.Format("{0} accepted invitation to '{1}' event", part.Name, ev.Name), DateTime.Now.ToShortDateString()));
                        _notifications.SendPush(ev.EventCreator.DeviceId, body);
                    }
                });
            }
            else
            {
                ExceptionsHelper.RiseError("User id and eventId are required values are: UId{0}, EId{1}", new[] { userUuid.ToString(CultureInfo.InvariantCulture), eventUuid.ToString() });
            }
        }
        public List<BestTankModel> GetBestTanks(long userId)
        {
            return _repository.GetBestTanks(userId);
        }

        #region private methods
        private void CreateParticipantsIfNotExists(long[] friends)
        {
            if (friends.Length > 0 && !_repository.ParticipantsExists(friends.ToList()))
            {
                _repository.CreateParticipants(GetUsersData(friends));
            }
        }
        private bool IsParticipantInList(long partId, IEnumerable<long> participants)
        {
            return participants.Contains(partId);
        }
        private bool IsParticipantInList(long partId, IEnumerable<ParticipantInfo> participants)
        {
            return participants.Any(p => p.ParticipantUuid == partId);
        }
        private List<BaseUserData> GetUsersData(long[] users)
        {
            var data = new List<BaseUserData>();
            if (users.Length > 100)
            {
                int amount = users.Length / 100;
                for (int i = 0; i < amount; i++)
                {
                    var hundredIds = users.Skip(i * 100).Take(100);
                    var result = GetUsersData(hundredIds.ToArray(), RequestType.BaseUserData);
                    if (result.Count > 0)
                    {
                        data.AddRange(result);
                    }
                }
            }
            else if (users.Length > 0)
            {
                var result = GetUsersData(users, RequestType.BaseUserData);
                if (result.Count > 0)
                {
                    data.AddRange(result);
                }
            }
            return data;
        }
        private List<BaseUserData> GetUsersData(long[] users, RequestType type)
        {
            string responce = _wotApiManager.GetWgBaseUserData(WgAppServiceHelper.GenerateBody(type, new object[] { string.Join(",", users) }));
            if (!responce.IsResponceSuccess() || users.Length == 0)
            {
                return new List<BaseUserData>();
            }
            return responce.GetObjectFromStringByKey(new BaseUserData(), users).Where(t => t != null).ToList();
        }
        private List<ClanEvent> GetClanEvents(long clanId, string mapId)
        {
            var resultList = new List<ClanEvent>();
            if (clanId > 0 && !string.IsNullOrEmpty(mapId))
            {
                string responce = _wotApiManager.GetClanEvents(WgAppServiceHelper.GenerateBody(RequestType.ClanEvents, new object[] { clanId, mapId }));
                if (!responce.IsResponceSuccess())
                {
                    return new List<ClanEvent>();
                }
                var result = responce.GetObjectFromStringByKey(new List<ClanEvent>(), new[] { clanId.ToString(CultureInfo.InvariantCulture) });
                foreach (var items in result)
                {
                    resultList.AddRange(items);
                }
            }
            return resultList;
        }
        private ClanData GetClanInfo(long clanId)
        {
            var clanData = new ClanData();
            if (clanId > 0)
            {
                string responce = _wotApiManager.GetClanInfo(WgAppServiceHelper.GenerateBody(RequestType.ClanData, new object[] { clanId }));
                if (!responce.IsResponceSuccess())
                {
                    return clanData;
                }
                var members = responce.GetDinamicObjectFromStringByKey(new ClanMember(), new[] { clanId.ToString(CultureInfo.InvariantCulture), MembersKey });
                clanData = responce.GetObjectFromStringByKey(new ClanData(), clanId.ToString(CultureInfo.InvariantCulture));
                clanData.ClanMembers = members;
            }
            return clanData;
        }
        private List<ProvinceInfo> GetProvinceInfo(List<string> provinces, string mapId)
        {
            if (provinces != null && provinces.Any() && !string.IsNullOrEmpty(mapId))
            {
                string responce = _wotApiManager.GetProvincesInfo(WgAppServiceHelper.GenerateBody(RequestType.ProvinceInfo, new object[] { mapId, string.Join(",", provinces) }));
                if (!responce.IsResponceSuccess())
                {
                    return new List<ProvinceInfo>();
                }
                return responce.GetObjectFromStringByKey(new ProvinceInfo(), provinces.ToArray());
            }
            return new List<ProvinceInfo>();
        }
        private void SetAvgDamageAndWinRate(IEnumerable<BaseUserData> parts)
        {
            foreach (BaseUserData baseUserData in parts)
            {
                var shots = baseUserData.statistics.all.shots;
                var dmgAmount = baseUserData.statistics.all.damage_dealt;
                if (shots != null && dmgAmount != null)
                {
                    baseUserData.AvgDamage = (dmgAmount / shots).Value;
                }
                var battles = baseUserData.statistics.all.battles;
                var wins = baseUserData.statistics.all.wins;
                if (battles != null && wins != null)
                {
                    baseUserData.WinRate = (wins * 100 / battles).Value;
                }
            }
        }
        private void SetBestTanksInfo(long accId, List<long> tanks)
        {
            var stats = GetTankStatistics(accId, tanks.ToArray());
            var info = GetTankInfo(tanks.ToArray());
            var bestTanks = new List<BestTankModel>();
            foreach (TankData tankData in info)
            {
                var btStat = stats.SingleOrDefault(t => t.tank_id == tankData.tank_id);
                if (btStat != null)
                {
                    int? battles = btStat.all.battles;
                    var bt = new BestTankModel();
                    bt.ImgUrl = tankData.image;
                    bt.Owner = accId;
                    bt.Name = tankData.localized_name;
                    bt.Uuid = tankData.tank_id;
                    bt.TankType = WgAppServiceHelper.GetTankType(tankData.type);
                    bt.ExpRate = btStat.all.battle_avg_xp ?? 0;
                    if (battles.HasValue)
                    {
                        if (btStat.all.damage_dealt != null)
                        {
                            bt.DamadgeRate = (btStat.all.damage_dealt / battles).Value;
                        }
                        if (btStat.all.wins != null)
                        {
                            bt.WinRate = (btStat.all.wins.Value * 100 / battles).Value;
                        }
                    }
                    bestTanks.Add(bt);
                }
            }
            _repository.SetBestTank(accId, bestTanks);
        }
        private List<TankStatistics> GetTankStatistics(long accId, long[] tankIds)
        {
            string responce = _wotApiManager.GetWgTanksStatistics(WgAppServiceHelper.GenerateBody(RequestType.TankStatistics, new object[] { accId, string.Join(",", tankIds), 0 }));
            if (!responce.IsResponceSuccess() || tankIds.Length == 0)
            {
                return new List<TankStatistics>();
            }
            return responce.GetObjectFromStringByKey(new List<TankStatistics>(), new[] { accId })[0].Where(t => t != null).ToList();
        }
        private IEnumerable<TankData> GetTankInfo(long[] tankIds)
        {
            string responce = _wotApiManager.GetWgTanksData(WgAppServiceHelper.GenerateBody(RequestType.TankData, new object[] { string.Join(",", tankIds) }));
            if (!responce.IsResponceSuccess() || tankIds.Length == 0)
            {
                return new List<TankData>();
            }
            return responce.GetDinamicObjectFromStringByKey(new TankData(), tankIds).Where(t => t != null).ToList();
        }

        #endregion
    }
}
