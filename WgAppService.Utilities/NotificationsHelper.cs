﻿using System;
using System.Configuration;
using PushSharp;
using PushSharp.Android;
using PushSharp.Core;

namespace WgAppService.Utilities
{
    public class NotificationsHelper
    {
        private readonly PushBroker _push;

        public NotificationsHelper()
        {
            _push = new PushBroker();
            //Wire up the events for all the services that the broker registers
            _push.OnNotificationSent += NotificationSent;
            _push.OnChannelException += ChannelException;
            _push.OnServiceException += ServiceException;
            _push.OnNotificationFailed += NotificationFailed;
            _push.OnDeviceSubscriptionExpired += DeviceSubscriptionExpired;
            _push.OnDeviceSubscriptionChanged += DeviceSubscriptionChanged;
            _push.OnChannelCreated += ChannelCreated;
            _push.OnChannelDestroyed += ChannelDestroyed;
        }
        public void SendPush(string deviceId, string jsonMessage)
        {
            _push.RegisterGcmService(new GcmPushChannelSettings(ConfigurationManager.AppSettings["PushNotificationsKey"]));

            _push.QueueNotification(new GcmNotification().ForDeviceRegistrationId(deviceId).WithJson(jsonMessage));
            //Stop and wait for the queues to drains
            _push.StopAllServices();
        }

        private void ChannelDestroyed(object sender)
        {
           
        }

        private void ChannelCreated(object sender, IPushChannel pushchannel)
        {
          
        }

        private void DeviceSubscriptionChanged(object sender, string oldsubscriptionid, string newsubscriptionid, INotification notification)
        {
            
        }

        private void DeviceSubscriptionExpired(object sender, string expiredsubscriptionid, DateTime expirationdateutc, INotification notification)
        {
           
        }

        private void NotificationFailed(object sender, INotification notification, Exception error)
        {
        }

        private void ServiceException(object sender, Exception error)
        {
        }

        private void ChannelException(object sender, IPushChannel pushchannel, Exception error)
        {
        }

        private void NotificationSent(object sender, INotification notification)
        {
        }
    }
}
