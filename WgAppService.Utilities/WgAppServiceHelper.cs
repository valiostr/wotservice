﻿using System;
using System.Configuration;
using Newtonsoft.Json.Linq;
using WgAppService.Utilities.Enumerations;

namespace WgAppService.Utilities
{
    public static class WgAppServiceHelper
    {
        public static string GenerateWotUrl(RequestType requestType)
        {
            string mainUrl = ConfigurationManager.AppSettings["GeneralApiUrlForWot"];
            string additionalUrlPart;
            switch (requestType)
            {
                case RequestType.BaseUserData:
                    additionalUrlPart = string.Format("{0}info/",ConfigurationManager.AppSettings["AccountUrl"]);
                    break;
                case RequestType.ClanData:
                    additionalUrlPart = string.Format("{0}info/",ConfigurationManager.AppSettings["ClansUrl"]);
                    break;
                case RequestType.ClanEvents:
                    additionalUrlPart = string.Format("{0}battles/",ConfigurationManager.AppSettings["GlobalWarUrl"]);
                    break;
                case RequestType.ProvinceInfo:
                    additionalUrlPart = string.Format("{0}provinces/",ConfigurationManager.AppSettings["GlobalWarUrl"]);
                    break;
                case RequestType.TankData:
                    additionalUrlPart = string.Format("{0}tankinfo/", ConfigurationManager.AppSettings["WikiUrl"]);
                    break;
                case RequestType.TankStatistics:
                    additionalUrlPart = string.Format("{0}stats/", ConfigurationManager.AppSettings["TanksUrl"]);
                    break;
                default:
                    return string.Empty;
            }
            return string.Format("{0}{1}", mainUrl, additionalUrlPart);
        }
        public static bool IsResponceSuccess(this string json)
        {
            JObject obj = JObject.Parse(json);
            string status = obj["status"].ToString();
            return status.Equals("ok");
        }
        private static string GetAppId()
        {
            return ConfigurationManager.AppSettings["ApplicationId"];
        }
        public static string GetFieldsForRequest(RequestType type)
        {
            switch (type)
            {
                case RequestType.BaseUserData:
                    return "account_id,nickname,last_battle_time,updated_at,statistics";
                case RequestType.ClanData:
                    return "clan_id,owner_id,members,members_count,name";
                case RequestType.ClanEvents:
                    return string.Empty;
                case RequestType.ProvinceInfo:
                    return "province_id,province_i18n";
                case RequestType.TankData:
                    return "image,short_name_i18n,tank_id,localized_name,name_i18n,type,level";
                case RequestType.TankStatistics:
                    return "all,account_id,tank_id";
                default:
                    return string.Empty;
            }
        }
        public static string GenerateBody(RequestType type , object[] parameters)
        {
            switch (type)
            {
                case RequestType.BaseUserData:
                    return string.Format("application_id={0}&language={1}&fields={2}&access_token={3}&account_id={4}", GetAppId(), string.Empty, GetFieldsForRequest(type), string.Empty, parameters[0]);
                case RequestType.ClanEvents:
                    return string.Format("application_id={0}&language={1}&fields={2}&map_id={4}&access_token={3}&clan_id={5}", GetAppId(), string.Empty, GetFieldsForRequest(type), string.Empty, parameters[0], parameters[1]);
                case RequestType.ClanData:
                    return string.Format("application_id={0}&language={1}&fields={2}&access_token={3}&clan_id={4}", GetAppId(), string.Empty, GetFieldsForRequest(type), string.Empty, parameters[0]);
                case RequestType.ProvinceInfo:
                    return string.Format("application_id={0}&language={1}&fields={2}&map_id={3}&province_id={4}", GetAppId(), string.Empty, GetFieldsForRequest(type), parameters[0], parameters[1]);
                case RequestType.TankData:
                    return string.Format("application_id={0}&language={1}&fields={2}&tank_id={3}", GetAppId(), string.Empty, GetFieldsForRequest(type),parameters[0]);
                case RequestType.TankStatistics:
                    return string.Format("application_id={0}&language={1}&fields={2}&access_token={3}&account_id={4}&tank_id={5}&in_garage={6}", GetAppId(), string.Empty, GetFieldsForRequest(type), string.Empty, parameters[0], parameters[1], parameters[2]);
                default:
                    return string.Empty;
            }

        }
        public static int GetTimeStamp()
        {
            var date = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second);
            return int.Parse((date.ToUniversalTime() - new DateTime(1970, 1, 1)).TotalSeconds.ToString());
        }
        public static int GetTimeStamp(DateTime dateT)
        {
            var date = new DateTime(dateT.Year, dateT.Month, dateT.Day, dateT.Hour, dateT.Minute, dateT.Second);
            return int.Parse((date.ToUniversalTime() - new DateTime(1970, 1, 1)).TotalSeconds.ToString());
        }
        public static DateTime GetDateByTimeStamp(int timestamp)
        {
            var dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            return dtDateTime.AddSeconds(timestamp).ToLocalTime();
        }
        public static int GetTankType(string type)
        {
            if (type.Contains("-"))
            {
                type = type.Remove(type.IndexOf('-'), 1).Trim();
            }
            return (int)Enum.Parse(typeof(TankTypes),type,true);
        }
    }
}
