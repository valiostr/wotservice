﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Script.Serialization;
using Newtonsoft.Json.Linq;

namespace WgAppService.Utilities
{
    public static class JsonSerializer
    {
        private static JavaScriptSerializer _javaScriptSerializer;
        private static string ParentKey = "data";
        public static void Serialize<T>(this T json, StringBuilder output)
        {
            _javaScriptSerializer = new JavaScriptSerializer();
            _javaScriptSerializer.Serialize(json, output);
        }

        public static T DeserializeJson<T>(this string json, T model)
        {
            _javaScriptSerializer = new JavaScriptSerializer();
            return _javaScriptSerializer.Deserialize<T>(json);
        }

        public static T GetObjectFromStringByKey<T>(this string json, T model, string childKey)
        {
            JObject obj = JObject.Parse(json);
            var parentObj = obj[ParentKey];
            return parentObj[childKey].ToObject<T>();
        }

        public static List<T> GetObjectFromStringByKey<T>(this string json, T model, string[] childKeys)
        {
            JObject obj = JObject.Parse(json);
            var parentObj = obj[ParentKey];
            return childKeys.Select(childKey => parentObj[childKey].ToObject<T>()).ToList();
        }
        public static List<T> GetObjectFromStringByKey<T>(this string json, T model, long[] childKeys)
        {
            JObject obj = JObject.Parse(json);
            var parentObj = obj[ParentKey];
            return childKeys.Select(childKey => parentObj[childKey.ToString()].ToObject<T>()).ToList();
        }
        public static List<T> GetDinamicObjectFromStringByKey<T>(this string json, T model, string[] childKey)
        {
            JObject obj = JObject.Parse(json);
            var parentObj = obj[ParentKey];
            var childObj = parentObj[childKey[0]][childKey[1]];
            return childObj.Children().Select(test => test.First.ToObject<T>()).ToList();
        }
        public static List<T> GetDinamicObjectFromStringByKey<T>(this string json, T model, long[] childKey)
        {
            JObject obj = JObject.Parse(json);
            var parentObj = obj[ParentKey];
            return parentObj.Children().Select(test => test.First.ToObject<T>()).ToList();
        }

        public static string GenerateJsonBody<T>(T model)
        {
            StringBuilder sb = new StringBuilder();
            Serialize(model, sb);
            return sb.ToString();
        }
    }
}
