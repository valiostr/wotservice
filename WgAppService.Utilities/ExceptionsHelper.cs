﻿using System;

namespace WgAppService.Utilities
{
    public static class ExceptionsHelper
    {
        public static void RiseError(string message, string[] param)
        {
            throw new Exception(string.Format(message, param));
        }
        public static void RiseError(string message, string param)
        {
            throw new Exception(string.Format(message, param));
        }
        public static void RiseError(string message)
        {
            throw new Exception(message);
        }
    }
}
