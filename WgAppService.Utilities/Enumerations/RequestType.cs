﻿namespace WgAppService.Utilities.Enumerations
{
    public enum RequestType
    {
        UserData,
        BaseUserData,
        ClanEvents,
        ClanData,
        ProvinceInfo,
        TankData,
        TankStatistics
    }
}


