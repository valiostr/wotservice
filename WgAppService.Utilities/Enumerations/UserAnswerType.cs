﻿namespace WgAppService.Utilities.Enumerations
{
    public enum UserAnswerType
    {
        Yes=0,
        No=1,
        Maybe=2
    }
}
