﻿namespace WgAppService.Utilities.Enumerations
{
    public enum ResponseStatus
    {
        Success = 200,
        Error,
        Failed = 400,
    }
}
