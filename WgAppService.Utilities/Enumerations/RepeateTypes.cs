﻿namespace WgAppService.Utilities.Enumerations
{
    public enum RepeateTypes
    {
        NoRepeat = 0,
        Daily = 1,
        Weekly = 2,
        EveryTwoWeeks = 3,
        Monthly = 4,
        HalfYear = 5,
        Yearly = 6
    }
}
