﻿namespace WgAppService.Utilities.Enumerations
{
    public enum TankTypes
    {
        Spg = 0,
        LightTank=1,
        MediumTank=2,
        HeavyTank=3,
        AtSpg=4,
    }
}
