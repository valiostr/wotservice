﻿using System.Runtime.Serialization;

namespace WgAppService.Contracts.DataContracts
{
    [DataContract]
    public class IncrementSyncData
    {
        [DataMember]
        public long UserId { get; set; }
        [DataMember]
        public int Timestamp { get; set; }
        [DataMember]
        public long ClanId { get; set; }
        [DataMember]
        public string MapId { get; set; }
    }
}
