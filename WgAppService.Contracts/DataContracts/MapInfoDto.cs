﻿using System.Runtime.Serialization;

namespace WgAppService.Contracts.DataContracts
{
    [DataContract]
    public class MapInfoDto
    {
        [DataMember]
        public int MapCode { get; set; }
        [DataMember]
        public string RuName { get; set; }
        [DataMember]
        public string EnName { get; set; }
    }
}
