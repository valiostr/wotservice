﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace WgAppService.Contracts.DataContracts
{
    [DataContract]
    public class CreateEventData
    {
        [DataMember]
        public Guid EventId { get; set; }
        [DataMember]
        public long UserId { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public int EventType { get; set; }
        [DataMember]
        public List<ParticipantInfoDto> Participants { get; set; }
        [DataMember]
        public string StartDate { get; set; }
        [DataMember]
        public string EndDate { get; set; }
        [DataMember]
        public int RepeatType { get; set; }
        [DataMember]
        public List<int> ExtraRepeatData { get; set; }
    }
}
