﻿using System.Runtime.Serialization;

namespace WgAppService.Contracts.DataContracts
{
    [DataContract]
    public class ParticipantInfoDto
    {
        [DataMember]
        public long ParticipantUuid { get; set; }
        [DataMember]
        public int Role { get; set; }
    }
}
