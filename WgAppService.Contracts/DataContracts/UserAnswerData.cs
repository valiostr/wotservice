﻿using System;
using System.Runtime.Serialization;

namespace WgAppService.Contracts.DataContracts
{
    [DataContract]
    public class UserAnswerData
    {
        [DataMember]
        public long UserId { get; set; }
        [DataMember]
        public Guid EventId { get; set; }
        [DataMember]
        public int Answer { get; set; }
    }
}
