﻿using System;

namespace WgAppService.Contracts.DataContracts
{
    public class DropEventData
    {
        public Guid EventId { get; set; }
        public long UserId { get; set; }
    }
}
