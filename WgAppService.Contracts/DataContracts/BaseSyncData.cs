﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace WgAppService.Contracts.DataContracts
{
    [DataContract]
    public class BaseSyncData
    {
        [DataMember]
        public long UserId { get; set; }
        [DataMember]
        public List<long> FriendsIds { get; set; }
        [DataMember]
        public int TimeStamp { get; set; }
        [DataMember]
        public string DeviceId { get; set; }
        [DataMember]
        public List<long> Tanks { get; set; }
    }
}
