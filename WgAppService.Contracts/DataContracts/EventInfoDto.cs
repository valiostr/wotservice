﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace WgAppService.Contracts.DataContracts
{
    [DataContract]
    public class EventInfoDto
    {
        [DataMember]
        public Guid EventId { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public ParticipantDto Owner { get; set; }
        [DataMember]
        public int EventType { get; set; }
        [DataMember]
        public int StartDate { get; set; }
        [DataMember]
        public int EndDate { get; set; }
        [DataMember]
        public int LastUpdated { get; set; }
        [DataMember]
        public int RepeatType { get; set; }
        [DataMember]
        public List<int> ExtraRepeatData { get; set; }
        [DataMember]
        public List<ParticipantDto> Participants { get; set; }
        [DataMember]
        public MapInfoDto Map { get; set; }
        [DataMember]
        public string Results { get; set; }
        [DataMember]
        public int? BattleType { get; set; }
        [DataMember]
        public bool EnableToEdit { get; set; }
    }
}
