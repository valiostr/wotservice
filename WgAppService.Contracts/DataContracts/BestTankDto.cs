﻿using System.Runtime.Serialization;

namespace WgAppService.Contracts.DataContracts
{
    [DataContract]
    public class BestTankDto
    {
        [DataMember]
        public int Uuid { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string ImgUrl { get; set; }
        [DataMember]
        public double WinRate { get; set; }
        [DataMember]
        public double SilverRate { get; set; }
        [DataMember]
        public double DamadgeRate { get; set; }
        [DataMember]
        public double ExpRate { get; set; }
        [DataMember]
        public int TankType { get; set; }
        [DataMember]
        public long Owner { get; set; }
    }
}
