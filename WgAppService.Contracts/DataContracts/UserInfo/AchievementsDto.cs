﻿using System.Runtime.Serialization;

namespace WgAppService.Contracts.DataContracts.UserInfo
{
    [DataContract]
    public class AchievementsDto
    {
        [DataMember]
        public int? TankExpertUk { get; set; }
        [DataMember]
        public int? MedalDumitru { get; set; }
        [DataMember]
        public int? Invader { get; set; }
        [DataMember]
        public int? MedalLehvaslaiho { get; set; }
        [DataMember]
        public int? Warrior { get; set; }
        [DataMember]
        public int? MedalHalonen { get; set; }
        [DataMember]
        public int? MedalPascucci { get; set; }
        [DataMember]
        public int? MedalOrlik { get; set; }
        [DataMember]
        public int? MedalBrothersInArms { get; set; }
        [DataMember]
        public int? Mousebane { get; set; }
        [DataMember]
        public int? TankExpertFrance { get; set; }
        [DataMember]
        public int? TankExpertJapan { get; set; }
        [DataMember]
        public int? MechanicEngineerUssr { get; set; }
        [DataMember]
        public int? MedalBrunoPietro { get; set; }
        [DataMember]
        public int? MedalDelanglade { get; set; }
        [DataMember]
        public int? LuckyDevil { get; set; }
        [DataMember]
        public int? Defender { get; set; }
        [DataMember]
        public int? ArmorPiercer { get; set; }
        [DataMember]
        public int? MedalKay { get; set; }
        [DataMember]
        public int? Supporter { get; set; }
        [DataMember]
        public int? MechanicEngineer { get; set; }
        [DataMember]
        public int? Steelwall { get; set; }
        [DataMember]
        public int? MaxSniperSeries { get; set; }
        [DataMember]
        public int? MedalKnispel { get; set; }
        [DataMember]
        public int? MedalBoelter { get; set; }
        [DataMember]
        public int? MedalEkins { get; set; }
        [DataMember]
        public int? MedalHeroesOfRassenay { get; set; }
        [DataMember]
        public int? MedalTamadaYoshio { get; set; }
        [DataMember]
        public int? TankExpertUsa { get; set; }
        [DataMember]
        public int? MechanicEngineerGermany { get; set; }
        [DataMember]
        public int? MaxPiercingSeries { get; set; }
        [DataMember]
        public int? TankExpert { get; set; }
        [DataMember]
        public int? IronMan { get; set; }
        [DataMember]
        public int? MedalRadleyWalters { get; set; }
        [DataMember]
        public int? Kamikaze { get; set; }
        [DataMember]
        public int? TankExpertGermany { get; set; }
        [DataMember]
        public int? Beasthunter { get; set; }
        [DataMember]
        public int? Sniper { get; set; }
        [DataMember]
        public int? MedalTarczay { get; set; }
        [DataMember]
        public int? MedalLavrinenko { get; set; }
        [DataMember]
        public int? MainGun { get; set; }
        [DataMember]
        public int? MedalOskin { get; set; }
        [DataMember]
        public int? MedalBurda { get; set; }
        [DataMember]
        public int? MedalBillotte { get; set; }
        [DataMember]
        public int? Huntsman { get; set; }
        [DataMember]
        public int? HandOfDeath { get; set; }
        [DataMember]
        public int? MedalFadin { get; set; }
        [DataMember]
        public int? MedalLafayettePool { get; set; }
        [DataMember]
        public int? MaxKillingSeries { get; set; }
        [DataMember]
        public int? TankExpertChina { get; set; }
        [DataMember]
        public int? MechanicEngineerUsa { get; set; }
        [DataMember]
        public int? MedalKolobanov { get; set; }
        [DataMember]
        public int? PattonValley { get; set; }
        [DataMember]
        public int? Bombardier { get; set; }
        [DataMember]
        public int? MechanicEngineerFrance { get; set; }
        [DataMember]
        public int? Sniper2 { get; set; }
        [DataMember]
        public int? MedalAbrams { get; set; }
        [DataMember]
        public int? MaxInvincibleSeries { get; set; }
        [DataMember]
        public int? MedalPoppel { get; set; }
        [DataMember]
        public int? MedalCrucialContribution { get; set; }
        [DataMember]
        public int? Raider { get; set; }
        [DataMember]
        public int? MaxDiehardSeries { get; set; }
        [DataMember]
        public int? MechanicEngineerUk { get; set; }
        [DataMember]
        public int? Invincible { get; set; }
        [DataMember]
        public int? Lumberjack { get; set; }
        [DataMember]
        public int? Sturdy { get; set; }
        [DataMember]
        public int? TitleSniper { get; set; }
        [DataMember]
        public int? Sinai { get; set; }
        [DataMember]
        public int? Diehard { get; set; }
        [DataMember]
        public int? MedalCarius { get; set; }
        [DataMember]
        public int? MedalLeClerc { get; set; }
        [DataMember]
        public int? TankExpertUssr { get; set; }
        [DataMember]
        public int? Evileye { get; set; }
        [DataMember]
        public int? MechanicEngineerJapan { get; set; }
        [DataMember]
        public int? MechanicEngineerChina { get; set; }
        [DataMember]
        public int? MedalNikolas { get; set; }
        [DataMember]
        public int? Scout { get; set; }

    }
}
