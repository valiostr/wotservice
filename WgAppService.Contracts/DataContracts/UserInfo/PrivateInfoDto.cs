﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace WgAppService.Contracts.DataContracts.UserInfo
{
    [DataContract]
    public class PrivateInfoDto
    {
        [DataMember]
        public string AccountType { get; set; }
        [DataMember]
        public int Gold { get; set; }
        [DataMember]
        public string FreeXp { get; set; }
        [DataMember]
        public long Credits { get; set; }
        [DataMember]
        public bool IsBoundToPhone { get; set; }
        [DataMember]
        public bool IsPremium { get; set; }
        [DataMember]
        public string PremiumExpiresAt { get; set; }
        [DataMember]
        public string AccountTypeI18N { get; set; }
        [DataMember]
        public List<long> Friends { get; set; }

    }
}
