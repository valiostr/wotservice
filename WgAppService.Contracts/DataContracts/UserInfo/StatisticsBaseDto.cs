﻿using System.Runtime.Serialization;

namespace WgAppService.Contracts.DataContracts.UserInfo
{
    [DataContract]
    public class StatisticsBaseDto
    {
        [DataMember]
        public int Spotted { get; set; }
        [DataMember]
        public int Hits { get; set; }
        [DataMember]
        public int BattleAvgXp { get; set; }
        [DataMember]
        public int Draws { get; set; }
        [DataMember]
        public int Wins { get; set; }
        [DataMember]
        public int Losses { get; set; }
        [DataMember]
        public int CapturePoints { get; set; }
        [DataMember]
        public int Battles { get; set; }
        [DataMember]
        public int DamageDealt { get; set; }
        [DataMember]
        public int HitsPercents { get; set; }
        [DataMember]
        public int DamageReceived { get; set; }
        [DataMember]
        public int Shots { get; set; }
        [DataMember]
        public int Xp { get; set; }
        [DataMember]
        public int Frags { get; set; }
        [DataMember]
        public int SurvivedBattles { get; set; }
        [DataMember]
        public int DroppedCapturePoints { get; set; }
    }
}
