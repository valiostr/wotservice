﻿using System.Runtime.Serialization;

namespace WgAppService.Contracts.DataContracts.UserInfo
{
    [DataContract]
    public class StatisticsDto
    {
        [DataMember]
        public StatisticsBaseDto All { get; set; }
        [DataMember]
        public StatisticsBaseDto Clan { get; set; }
        [DataMember]
        public StatisticsBaseDto Company { get; set; }
        [DataMember]
        public StatisticsBaseDto Historical { get; set; }
        [DataMember]
        public int? MaxXp { get; set; }
        [DataMember]
        public int? MaxDamage { get; set; }
        [DataMember]
        public int? MaxDamageVehicle { get; set; }

    }
}
