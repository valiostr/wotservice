﻿using System.Runtime.Serialization;

namespace WgAppService.Contracts.DataContracts.UserInfo
{
    [DataContract]
    public class BaseUserInfo
    {
        [DataMember]
        public long UserId { get; set; }
        [DataMember]
        public int LastBattleTime { get; set; }
        [DataMember]
        public string Nickname { get; set; }
        [DataMember]
        public int UpdatedAt { get; set; }
        [DataMember]
        public long AvgDamage { get; set; }
        [DataMember]
        public long WinRate { get; set; }
    }
}
