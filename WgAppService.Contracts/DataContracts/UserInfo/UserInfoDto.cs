﻿using System;
using System.Runtime.Serialization;

namespace WgAppService.Contracts.DataContracts.UserInfo
{
    [DataContract]
    public class UserInfoDto: BaseUserInfo
    {
        [DataMember]
        public AchievementsDto Achievements { get; set; }
        [DataMember]
        public string ClientLanguage { get; set; }
        [DataMember]
        public StatisticsDto Statistics { get; set; }
        [DataMember]
        public string AccountId { get; set; }
        [DataMember]
        public string CreatedAt { get; set; }
        [DataMember]
        public long GlobalRating { get; set; }
        [DataMember]
        public int? ClanId { get; set; }
        [DataMember]
        public string LogoutAt { get; set; }
        [DataMember]
        public PrivateInfoDto Private { get; set; }

    }
}
