﻿using System.Runtime.Serialization;

namespace WgAppService.Contracts.DataContracts
{
    [DataContract]
    public class ParticipantDto
    {
        [DataMember]
        public long UserId { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public int? UserAnswer { get; set; }
        [DataMember]
        public int? AnswerTime { get; set; }
        [DataMember]
        public bool? IsInvited { get; set; }
        [DataMember]
        public bool? IsCommander { get; set; }
        [DataMember]
        public int PlayerRole { get; set; }
    }
}
