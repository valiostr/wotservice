﻿using System.Runtime.Serialization;
using WgAppService.Utilities.Enumerations;

namespace WgAppService.Contracts.Messages
{
    [DataContract]
    public class ResponseBase
    {
        [DataMember]
        public ResponseStatus ResponseStatus;
        [DataMember]
        public string Message;
        [DataMember]
        public string Version;
        [DataMember]
        public string Build;
        [DataMember]
        public int TimeStamp { get; set; }
    }
}
