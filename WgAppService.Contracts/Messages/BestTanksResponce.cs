﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using WgAppService.Contracts.DataContracts;

namespace WgAppService.Contracts.Messages
{
    public class BestTanksResponce:ResponseBase
    {
        [DataMember]
        public List<BestTankDto> BestTanks { get; set; } 
    }
}
