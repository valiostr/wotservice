﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using WgAppService.Contracts.DataContracts.UserInfo;

namespace WgAppService.Contracts.Messages
{
    public class UserInfoResponse : ResponseBase
    {
        [DataMember]
        public List<BaseUserInfo> BasicUserInfo { get; set; }

        public UserInfoResponse()
        {
            BasicUserInfo = new List<BaseUserInfo>();
        }
    }
}
