﻿using System;
using System.Runtime.Serialization;

namespace WgAppService.Contracts.Messages
{
    public class CreateEventResponse:ResponseBase
    {
        [DataMember]
        public Guid EventId { get; set; }

    }
}
