﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using WgAppService.Contracts.DataContracts;
using WgAppService.Contracts.DataContracts.UserInfo;

namespace WgAppService.Contracts.Messages
{
    public class SyncDataResponse : ResponseBase
    {
        [DataMember]
        public List<BaseUserInfo> ClanMembersData { get; set; }
        [DataMember]
        public List<EventInfoDto> EventPlatoonsData { get; set; }
        [DataMember]
        public List<EventInfoDto> EventCompanysData { get; set; }
        [DataMember]
        public List<EventInfoDto> EventClanWarsData { get; set; }

        public SyncDataResponse()
        {
            ClanMembersData = new List<BaseUserInfo>();
            EventClanWarsData = new List<EventInfoDto>();
            EventCompanysData = new List<EventInfoDto>();
            EventPlatoonsData = new List<EventInfoDto>();
        }
    }
}
