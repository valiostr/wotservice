﻿using System;
using System.ServiceModel;
using System.ServiceModel.Web;
using WgAppService.Contracts.DataContracts;
using WgAppService.Contracts.Messages;

namespace WgAppService.Contracts
{
    [ServiceContract]
    public interface IWgAppService
    {
        [OperationContract]
        [WebInvoke(UriTemplate = "SyncUserData", ResponseFormat = WebMessageFormat.Json, Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped)]
        UserInfoResponse SyncUserData(BaseSyncData data);
        [OperationContract]
        [WebInvoke(UriTemplate = "IncrementSync", ResponseFormat = WebMessageFormat.Json, Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped)]
        SyncDataResponse IncrementSync(IncrementSyncData data);

        [OperationContract]
        [WebInvoke(UriTemplate = "CreateEvent", ResponseFormat = WebMessageFormat.Json, Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped)]
        CreateEventResponse CreateEvent(CreateEventData data);
        
        [OperationContract]
        [WebInvoke(UriTemplate = "UpdateEvent", ResponseFormat = WebMessageFormat.Json, Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped)]
        ResponseBase UpdateEvent(CreateEventData data);

        [OperationContract]
        [WebInvoke(UriTemplate = "DropEvents", ResponseFormat = WebMessageFormat.Json, Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped)]
        ResponseBase DropEvents(DropEventData data);

        [OperationContract]
        [WebInvoke(UriTemplate = "SetUserAnswer", ResponseFormat = WebMessageFormat.Json, Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped)]
        ResponseBase SetUserAnswer(UserAnswerData data);

        [OperationContract]
        [WebInvoke(UriTemplate = "GetBestTanks", ResponseFormat = WebMessageFormat.Json, Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped)]
        BestTanksResponce GetBestTanks(BaseSyncData data);
    }
}
